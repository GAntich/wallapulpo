<?php
/*
 * Archivo de configuraci�n de las clases que usaremos
 * Se llama desde Configure::getClass('NombreClase');
 */

/**
 * Engine:  Don't modify.
 */
//$config['factory']						=  PATH_ENGINE . 'factory.class.php';
//$config['sql']							=  PATH_ENGINE . 'sql.class.php';


$config['mail']							=  PATH_ENGINE . 'mail.class.php';
$config['session']						=  PATH_ENGINE . 'session.class.php';
$config['user']							=  PATH_ENGINE . 'user.class.php';
$config['url']							=  PATH_ENGINE . 'url.class.php';
$config['uploader']						=  PATH_ENGINE . 'uploader.class.php';


$config['dispatcher']					=  PATH_ENGINE . 'dispatcher.class.php';


/** 
 * Controllers & Models
 *
 */

// 404 Controller
$config['ErrorError404Controller']		            = PATH_CONTROLLERS . 'error/error404.ctrl.php';

// Home Controller
$config['HomeHomeController']			            = PATH_CONTROLLERS . 'home/home.ctrl.php';

//Demo Controller
$config['DemoDemoController']			            = PATH_CONTROLLERS . 'demo/demo.ctrl.php';

//Register Controller
$config['RegisterRegisterController']			    = PATH_CONTROLLERS . 'register/register.ctrl.php';

$config['HomeDatabaseModel']                        = PATH_MODELS . 'home/homedatabase.model.php';

//Register Controller
$config['RegisterActivationController']			    = PATH_CONTROLLERS . 'register/activation.ctrl.php';

//Logout Controller
$config['LogoutController']			                = PATH_CONTROLLERS . 'logout/logout.ctrl.php';

//Login Controller
$config['LoginController']			                = PATH_CONTROLLERS . 'login/login.ctrl.php';

//Add money controller
$config['PursePurseController']			            = PATH_CONTROLLERS . 'purse/purse.ctrl.php';

//User controller
$config['UserController']			                = PATH_CONTROLLERS . 'user/user.ctrl.php';
$config['RetrieveUserController']			        = PATH_CONTROLLERS . 'user/retrieve.ctrl.php';

//Product Controller
$config['ProductProductController']			        = PATH_CONTROLLERS . 'product/product.ctrl.php';
$config['ProductListController']			        = PATH_CONTROLLERS . 'product/list.ctrl.php';
$config['ProductNewController']                     = PATH_CONTROLLERS . 'product/new.ctrl.php';
$config['ProductEditController']                    = PATH_CONTROLLERS . 'product/edit.ctrl.php';

//Forgot Password Controller
$config['ForgotpasswordController']			        = PATH_CONTROLLERS . 'forgotpassword/forgotpassword.ctrl.php';

//Search Controller
$config['SearchController']			                = PATH_CONTROLLERS . 'search/search.ctrl.php';

//Mail Controller
$config['MailMailController']			            = PATH_CONTROLLERS . 'mail/mail.ctrl.php';


// Shared controllers
$config['SharedHeadController']			            = PATH_CONTROLLERS . 'shared/head.ctrl.php';
$config['SharedFooterController']		            = PATH_CONTROLLERS . 'shared/footer.ctrl.php';


