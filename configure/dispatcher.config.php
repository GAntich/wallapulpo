<?php
/*
 * Cada ruta SEO images mapeada a un controlador que se encuentra en el 'base.config.php'
 */

$config['default']				= 'HomeHomeController';
$config['home']					= 'HomeHomeController';
$config['demo']                 = 'DemoDemoController';
$config['register']             = 'RegisterRegisterController';
$config['activation']           = 'RegisterActivationController';
$config['logout']               = 'LogoutController';
$config['login']                = 'LoginController';
$config['purse']                = 'PursePurseController';
$config['product']              = 'ProductProductController';
$config['list']                 = 'ProductListController';
$config['search']               = 'SearchController';
$config['new']                  = 'ProductNewController';
$config['edit']                 = 'ProductEditController';
$config['user']                 = 'UserController';
$config['retrieve']             = 'RetrieveUserController';
$config['forgotpassword']       = 'ForgotpasswordController';
$config['mail']                 = 'MailMailController';



