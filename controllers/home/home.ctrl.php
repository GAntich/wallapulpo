<?php
/**
 * Home Controller: Controller example.

 */
class HomeHomeController extends Controller
{
	protected $view = 'home/home.tpl';

	public function build()
	{
		$params = $this->getParams();
		if(isset($params['url_arguments'][0])){
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}else{
			$this->setLayout( $this->view );

			//Último producto introducido en la web
			$obj = $this->getClass('HomeDatabaseModel');
			$data = $obj->getLastProduct();
			if(!empty($data)) {
				$this->assign('lastProductFlag', true);
				$this->assign('img', $data[0]['imagen']);
				$this->assign('precio', $data[0]['precio']);
				$link = 'http://g6.dev/product/' . $data[0]['url'];
				$this->assign('link', $link);
				$this->assign('nombre', $data[0]['nombre']);
			}else{
				$this->assign('lastProductFlag', false);
			}

			//Los 5 más visionados!
			$obj = $this->getClass('HomeDatabaseModel');
			$mostViewed = $obj->getMostViewed(5);
			if(!empty($mostViewed)){
				$this->assign('mostViewedFlag', true);
				for ($i = 0; $i < count($mostViewed); $i++) {
					if(strlen($mostViewed[$i]['descripcion'])>50){
						$mostViewed[$i]['descripcion'] = substr($mostViewed[$i]['descripcion'], 3, 50). "...";
					}
				}
				$this->assign('mostViewed', $mostViewed);
			}else {
				$this->assign('mostViewedFlag', false);
			}

			//Ultimas 5 imagenes
			$obj = $this->getClass('HomeDatabaseModel');
			$lastImages = $obj->getLastImages();
			if(!empty($lastImages)) {
				$this->assign('lastImagesFlag', true);
				for ($i = 0; $i < count($lastImages); $i++) {
					$imgs[$i]['imagen'] = str_replace('400x300', '100x100', $lastImages[$i]['imagen']);
				}
				$this->assign('lastImages', $imgs);
			}else{
				$this->assign('lastImagesFlag', false);
			}


			//Ultimas imagenes subidas (tamaño 100x100)

			//Twitter.
			require_once('twitterAPI/twitterAPIExchange.php');
			$twittersettings = array(
				'oauth_access_token' => "724583745244794880-2BTJaLmiE6LZUYMsRYbOHij5G1HmTEg",
				'oauth_access_token_secret' => "r2blFNeCX9IkRnu9SGm0jM6CucYgPeeAYVZOaNx6tVcLy",
				'consumer_key' => "dxI8wajO9hliLnSAhH3kmo18q",
				'consumer_secret' => "W5tnfCnL2HpEdeE2kE0BHYyAJPLbre7lS1XiWShrGvevPCQB7B"
			);

			$url = "https://api.twitter.com/1.1/search/tweets.json";
			$requestMethod = 'GET';
			$getfields = '?q=%23LSStore&result_type=recent';
			$twitter = new TwitterAPIExchange($twittersettings);
			$string = json_decode($twitter->setGetfield($getfields)
				->buildOauth($url, $requestMethod)
				->performRequest(),$assoc = TRUE);
			$i =0;
			$twits = array();

			foreach($string as $items)
			{
				if($i==0){
					foreach($items as $idem){
						$datos = array();

						$datos['text'] = $idem['text'];
						$datos['name'] = $idem['user']['name'];
						$datos['screen_name'] = $idem['user']['screen_name'];
						$datos['profile_image_url'] = $idem['user']['profile_image_url'];

						array_push($twits,$datos);

					}
				}
				$i++;
			}
			$this->assign('twits',$twits);
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}