<?php
/**
 * Home Controller: Controller example.

 */
class LogoutController extends Controller
{
	protected $view = 'logout/logout.tpl';

	public function build()
	{

		$this->setLayout( $this->view );
		$params = $this->getParams();

		if(isset($params['url_arguments'][0])){
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}else{
			Session::getInstance()->set('logged', false);
			$this->setLayout( 'logout/logout.tpl' );
			header('Refresh: 5; URL=http://g6.dev');
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}