<?php

class SharedHeadController extends Controller
{
	public function build( )
	{
		$this->setLayout( 'shared/head.tpl' );

		$this->assign('logged',Session::getInstance()->get('logged'));
		$nombre = '';
		$password = '';
		$img = '';
		$obj = $this->getClass('HomeDatabaseModel');
		if ($user = Session::getInstance()->get('logged')) {
			$nombre = Session::getInstance()->get('username');
			$password = Session::getInstance()->get('password');
			$data=$obj->getMoney($nombre);
			$money = $data[0]['dineroActual'];
			$id = $obj->getIdUser($nombre,$password);
			$img = $obj->imageUserfromid($id);
			$unread = $obj->hasUnreadMessages($id[0]['id'])[0]['count(unread)'];

			$this->assign('unread',"(".$unread.")");
		}else{
			$money = 0;
		}
		$this->assign('nombre',$nombre);
		$this->assign('imagen',$img);
		$this->assign('money',$money);
	}
}
?>
