<?php
/**
 * Home Controller: Controller example.

 */
class PursePurseController extends Controller
{
	protected $view = 'purse/purse.tpl';

	public function build()
	{
		//Asignamos la vista
		$this->setLayout( $this->view );
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][0])){
			if($params['url_arguments'][0]=='confirm'){
				$obj = $this->getClass('HomeDatabaseModel');
				$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
				if(!isset($_SESSION['dineroActual'])){
					$this->setLayout( 'error/error404.tpl' );
					header("HTTP/1.1 404 Not Found");
				}else{
					$dineroActual=Session::getInstance()->get('dineroActual');
					if($dineroActual == false){
						$this->setLayout( 'error/error404.tpl' );
						header("HTTP/1.1 404 Not Found");
					}else{
						if($telegram != false){
							$this->assign('telegram',true);
							$submit = Filter::getString( 'submit' );
							if ($submit === 'Enviar') {
								$code = Filter::getString( 'codeTelegram' );
								$nombre = Session::getInstance()->get('username');
								$password = Session::getInstance()->get('password');
								$obj = $this->getClass('HomeDatabaseModel');
								$codeBase = $obj->getCodeTelegram($nombre,$password);
								if($code==$codeBase[0]['codigoTelegram']){
									$user = Session::getInstance()->get('username');
									$dineroActual=Session::getInstance()->get('dineroActual');
									$result = $obj->insertarSaldo($user,$dineroActual);
									Session::getInstance()->set('dineroActual',false);
									if ($result) {
										header('Location: http://g6.dev');
									}else{
										$this->setLayout( 'error/error404.tpl' );
										header("HTTP/1.1 404 Not Found");
									}
								}else{
									$this->assign('legendTelegram','Este no es el código!');
								}
							}
						}else{
							$this->setLayout( 'error/error404.tpl' );
							header("HTTP/1.1 404 Not Found");
						}
					}
				}
			}else{
				$this->setLayout( 'error/error404.tpl' );
				header("HTTP/1.1 404 Not Found");
			}
		}else{
			$this->assign('telegram',false);
			$error=0;
			$legendImporte = '';
			$legendError = '';
			$legendMetodo = '';
			if($user = Session::getInstance()->get('logged')){
				$submit = Filter::getString( 'submit' );
					$obj = $this->getClass('HomeDatabaseModel');
					$idComprador = $obj->getIdUser(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
					$compras = $obj->getCompras($idComprador[0]['id']);
					$listaNoActivos = array();
					$listaActivos = array();
					//Los clasificamos según si siguen en venta o no:
					for ($i =0 ; $i < count($compras); $i++) {
						$pr = $obj->getProduct($compras[$i]['producto']);
						$nombreusuario = $obj->usernamefromid($pr[0]['idusuario']);
						if($obj->comprobarProducto($pr[0]['url'])){
							$obj = $this->getClass('HomeDatabaseModel');
							$pr[0]['idusuario'] = $nombreusuario[0]['username'];
							array_push($listaActivos, $pr[0]);
						}else{
							$pr[0]['idusuario'] = $nombreusuario[0]['username'];
							array_push($listaNoActivos, $pr[0]);
						}
					}
					if(!empty($compras)){
						$this->assign('activos', $listaActivos);
						$this->assign('inactivos', $listaNoActivos);
						$this->assign('flagCompras',true);
					}else{
						$this->assign('flagCompras',false);
					}
					if ($submit === 'Enviar') {
						$importe = Filter::getString( 'importe' );

						$obj = $this->getClass('HomeDatabaseModel');
						$user = Session::getInstance()->get('username');
						$saldo = $obj->saldoDisponible($user);
						$dineroActual = $saldo[0]['dineroActual'] + $importe;
						if($importe >100){
							$error = 1;
							$legendImporte = 'No puedes transferir más de 100 € a la vez.';
						}
						if($importe<1){
							$error = 1;
							$legendImporte = 'Deber transferir por lo menos 1 €.';
							if($importe == 0)	$legendImporte = 'Debes transferir algo.';
						}
						if( ( $saldo[0]['dineroActual'] + $importe) > 1000){
							$legendError = 'No puedes tener más de 1000 € en tu cuenta.';
							$error=1;
						}

						//Comprovamos el método de pago
						$metodo = Filter::getString( 'metodo' );
						if($metodo == "" || $metodo ==null){
							$legendMetodo = 'Debes escoger un método.';
							$error=1;
						}
						if($error==0){
							$obj = $this->getClass('HomeDatabaseModel');
							$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
							if($telegram != false){
								$codigoRandom = '';
								$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
								$max = strlen($pattern)-1;
								for($i=0;$i < 8;$i++) $codigoRandom .= $pattern{mt_rand(0,$max)};
								$obj->updateCodeTelegram($codigoRandom, Session::getInstance()->get('username'), Session::getInstance()->get('password'));
								$queryArray = [
									'chat_id' => $telegram[0]['chatTelegram'],
									'text' => 'Para recargar, introduce el siguiente código: '.$codigoRandom,
								];
								$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?'.http_build_query($queryArray);
								file_get_contents($url);
								header('Location: http://g6.dev/purse/confirm');
								Session::getInstance()->set('dineroActual', $dineroActual);
							}else{
								Session::getInstance()->set('dineroActual', false);
								$result = $obj->insertarSaldo($user,$dineroActual);
								if ($result) {
									header('Location: http://g6.dev');
								}else{
									$this->setLayout( 'error/error404.tpl' );
								}
							}
						}
						$this->assign('legendError',$legendError);
						$this->assign('legendImporte',$legendImporte);
						$this->assign('legendMetodo',$legendMetodo);
					}
				if($submit === 'Buscar'){
					$busc = Filter::getString('busqueda');
					header('Location: http://g6.dev/search/'.$busc.'/1');
				}
			}else{

				$this->setLayout( 'error/error403.tpl' );
				header("HTTP/1.1 403 Forbidden");
			}
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}
