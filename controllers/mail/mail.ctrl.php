<?php
/**
 * Home Controller: Controller example.

 */
class MailMailController extends Controller
{
	protected $view = 'mail/mail.tpl';

	public function build()
	{
		$this->setLayout( $this->view );
		$params = $this->getParams();
		$obj = $this->getClass('HomeDatabaseModel');
		$this->assign('sublayout','default');
		if (Session::getInstance()->get('logged')) {

			$session = $obj->getIdUser(Session::getInstance()->get('username'),Session::getInstance()->get('password'));

			if (!isset($params['url_arguments'][0])) {
				//Caso pagina principal
				//Guardamos el ID del usuario

				//Nos descargamos los mensajes
				$conversations = $obj->getMyConversations($session[0]['id']);
				//Creamos las variables de leidos y no leidos
				$sent = array();
				$received = array();
				foreach ($conversations as $conversation){
					if($conversation['user1'] == $session[0]['id']){//Si yo he iniciado la conversacion
						$message = $obj->getLastMessageAndFlags($conversation['conversation']); //recoger ultimo mensage y de quien a quien va

						$conversation['user2'] = $obj->usernamefromid($conversation['user2'])[0]['username']; //Almaceno el nombre de aquien iba dirigido el primer mensaje
						$conversation['message'] = $message[0]['message'];//Guardo el mensaje
						if($message[0]['user2'] == $session[0]['id']){//Si a quien va dirigido el mensaje soy yo
							if($message[0]['unread'] == 0)$conversation['unread']=false;
							else$conversation['unread']=true;
						}else{
							$conversation['unread'] = false; //Si no va dirigido a mi no me muestres el flag
						}
						array_push($sent,$conversation);
					}else{
						$message = $obj->getLastMessageAndFlags($conversation['conversation']); //recoger ultimo mensaje y de quien aquien
						$conversation['user1'] = $obj->usernamefromid($conversation['user1'])[0]['username']; ///Almaceno el nombre de quien empezo la ocnversacion
						$conversation['message'] = $message[0]['message']; //Guardo el mensaje
						if($message[0]['user2'] == $session[0]['id']){ //Si a quien va dirigido el mensaje soy yo
							if($message[0]['unread'] == 0)$conversation['unread']=false;
							else$conversation['unread']=true;
						}else{
							$conversation['unread'] = false;//Si no va dirigido a mi no me muestres falg
						}
						array_push($received,$conversation);
					}
				}
				$this->assign('sent',$sent);
				$this->assign('received',$received);

			} else if(isset($params['url_arguments'][0]) && $params['url_arguments'][0] == 'send' && isset($params['url_arguments'][1])) {

				$this->assign('sublayout','send');
				//Creamos un mensaje nuevo para urlarg1 viniendo desde su perfil.
				if($obj->existeUser($params['url_arguments'][1])){//Comprobar existencia de user por medio del nombre
					$this->assign('to',$params['url_arguments'][1]);
					$error = false;
					$submit = Filter::getString('submit');
					if ($submit === 'Enviar') {
						$title = Filter::getString('title');
						if ((strlen($title)) > 50) {
							$this->assign('legendTitle','Como maximo debe tener 50 carácteres.');
							$error = true;
						} else {
							if ((strlen($title)) < 1) {
								$this->assign('legendTitle','El título es obligatorio.');
								$error = true;
							}
						}
						$message = Filter::getString('src_New');
						if ($message == "<p><br></p>") {
							$this->assign('legendMessage','El mensaje debe decir algo.');
							$error = true;
						}
						if(!$error){
							$conversation = str_replace(" ", "_", $title);
							$unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
								'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
								'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
								'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
								'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '`'=> '_', '´'=>'_', '^'=>'', '!'=>'','"'=>'', '·'=>'.', '$'=>'S', '%'=>'_',
								'&'=>'', '/'=>'_', '('=>'_', ')'=>'_', '='=>'_', '\''=>'_', '|'=>'_', '@'=>'a', '#'=>'_', '#'=>'_', '¢'=>'_','∞'=>'_', '¬'=>'_', '÷'=>'_',
								'“'=>'_', '”'=>'_','≠'=>'_');
							$conversation = strtr($conversation, $unwanted_array );
							$obj->newMessageinConversation($conversation,$title,$session[0]['id'],$obj->getIdfromUsername($params['url_arguments'][1])[0]['id'],$message,date('Y-m-d'));
							header('Location: http://g6.dev/mail/'.$conversation);

						}
					}
				}else{
					$this->setLayout( 'error/error404.tpl' );
					header("HTTP/1.1 404 Not Found");
				}
			}else if(isset($params['url_arguments'][0])){//Caso visionado de mensaje
				$conversation = $obj->existsConversation($params['url_arguments'][0]);
				if($conversation != null){
					$obj->setReadToFalse($params['url_arguments'][0],$session[0]['id']);

					$user1 = $conversation[0]['user1'];
					$user2 = $conversation[0]['user2'];

					$this->assign('sublayout','view');

					for($i = 0; $i<count($conversation); $i++){
						$conversation[$i]['user1'] = $obj->usernamefromid($conversation[$i]['user1'])[0]['username'];
						$conversation[$i]['user2'] = $obj->usernamefromid($conversation[$i]['user2'])[0]['username'];
					}

					$this->assign('conversation',$conversation);
					$this->assign('me',Session::getInstance()->get('username'));


					$submit = Filter::getString('submit');
					if ($submit === 'Enviar') {
						$message = Filter::getString('src_Msg');
						if ($message == "<p><br></p>") {
							$this->assign('legendMessage','El mensaje debe decir algo.');
						}else{
							if($user1 == $session[0]['id']){ //Soy yo quien envio el mensaje
								$obj->newMessageinConversation($params['url_arguments'][0],$conversation[0]['title'],$user1,$user2,$message,date('Y-m-d'),0,1);
							}else {
								$obj->newMessageinConversation($params['url_arguments'][0], $conversation[0]['title'], $user2, $user1, $message, date('Y-m-d'),0,1);
							}
							header('Location: http://g6.dev/mail/'.$params['url_arguments'][0]);
						}
					}
				}else{
					$this->setLayout( 'error/error404.tpl' );
					header("HTTP/1.1 404 Not Found");
				}

			}else{
				$this->setLayout( 'error/error404.tpl' );
				header("HTTP/1.1 404 Not Found");
			}
		}else{
			$this->setLayout( 'error/error403.tpl' );
			header("HTTP/1.1 403 Forbidden");
		}

	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}