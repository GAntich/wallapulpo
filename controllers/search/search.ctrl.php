<?php
/**
 * Home Controller: Controller example.

 */
class SearchController extends Controller
{
	protected $view = 'search/search.tpl';

	public function build()
	{
		$params = $this->getParams();
		$this->setLayout($this->view);
		if(isset($params['url_arguments'][0]) && isset($params['url_arguments'][1]) && !isset($params['url_arguments'][2])) {
			$obj = $this->getClass('HomeDatabaseModel');
			$listaProductos = $obj->listProducts();
			$listaEncontrados = array();
			for ($i =0 ; $i < count($listaProductos); $i++) {
				$palabras = explode(" ", $listaProductos[$i]['nombre']);
				$factorExito = $obj->getFactorExito($listaProductos[$i]['idusuario'])[0]['count(*)'];
				if($factorExito<5){
					$listaProductos[$i]['factor']= 1;
				}else if($factorExito<10){
					$listaProductos[$i]['factor']= 2;
				}else if($factorExito<100){
					$listaProductos[$i]['factor']= 3;
				}else{
					$listaProductos[$i]['factor']= 4;
				}

				for ($j =0 ; $j < count($palabras); $j++) {
					if($palabras[$j]==$params['url_arguments'][0]){
						array_push($listaEncontrados, $listaProductos[$i]);
					}
				}
			}

			//paginamos
			$entera = (count($listaEncontrados) - count($listaEncontrados) % 10) / 10;

			$resto = count($listaEncontrados) %10;

			if($resto==0) 	$paginas = $entera;
			else			$paginas = $entera+1;


			if ($params['url_arguments'][1] <= $paginas && $params['url_arguments'][1] > 0) {
				if ($params['url_arguments'][1] == $paginas) {
					$fin = count($listaEncontrados) % 10;
					if (count($listaEncontrados) == 10) {
						$fin = 10;
					}

				} else    $fin = 10;
				$incremento = 0;
				for ($i = ($params['url_arguments'][1] - 1) * 10; $i < ($params['url_arguments'][1] - 1) * 10 + $fin; $i++) {

					$listaAEnviar[$incremento] = $listaEncontrados[$i];
					$incremento++;
				}
				$this->assign('notfound', false);
				$this->assign('productos', $listaAEnviar);
				$this->assign('paginas', $paginas);
				$this->assign('search', $params['url_arguments'][0]);

				$this->assign('actual', $params['url_arguments'][1]);

			} else {
				$this->assign('notfound', true);
			}
		}else{
			$this->setLayout('error/error404.tpl');
			header("HTTP/1.1 404 Not Found");
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}