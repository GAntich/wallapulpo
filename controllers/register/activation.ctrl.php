<?php
/**
 * Home Controller: Controller example.

 */
class RegisterActivationController extends Controller
{
	protected $view = 'register/activation.tpl';

	public function build()
	{
		//Asignamos la vista
		$this->setLayout( $this->view );
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][1])){
			//Cambia template y indica el error con el código.
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}else{
			$this->assign('activation',false);
			$this->assign('registered',false);
			$this->assign('errorcode',false);
			if(isset($params['url_arguments'][0])){
				//Comprovamos que el código de activación sea correcto.
				$obj = $this->getClass('HomeDatabaseModel');
				$data = $obj->existe('codigoActivacion',$params['url_arguments'][0]);
				if($data==true){
					//Comprobamos que el usuario no haya validado ya el código.
					$obj = $this->getClass('HomeDatabaseModel');
					$data = $obj->estaRegistrado($params['url_arguments'][0]);
					if($data!=false){
						//Completamos el registro y lo mandamos a la home.
						$this->assign('activation',true);
						$obj->registroCompletado($params['url_arguments'][0]);
						Session::getInstance()->set('logged', true);
						Session::getInstance()->set('username', $data[0]['username']);
						Session::getInstance()->set('password', $data[0]['pass']);
						header('Refresh: 5; URL=http://g6.dev');

						$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
						if($telegram != false){
							$queryArray = [
								'chat_id' => $telegram[0]['chatTelegram'],
								'text' => 'Tu cuenta ha sido activada.',
							];
							$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?'.http_build_query($queryArray);
							file_get_contents($url);
						}
					}else{
						$this->assign('registered',true);
					}
				}else{
					$this->assign('errorcode',true);
				}
			}else{
				$this->setLayout( 'error/error404.tpl' );
				header("HTTP/1.1 404 Not Found");
			}

		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}