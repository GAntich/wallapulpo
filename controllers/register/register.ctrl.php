<?php
/**
 * Home Controller: Controller example.

 */
class RegisterRegisterController extends Controller
{
	protected $view = 'register/register.tpl';

	public function build()
	{
		//Asignamos la vista
		$this->setLayout( $this->view );
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][0])){
			//Cambia template y indica el error con el código.
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}else{
			$encontradoArroba = 0;
			$encontradoPunto = 0;
			$encontradoMinuscula = 0;
			$encontradoMayuscula = 0;
			$encontradoNumero = 0;
			$color = 'white';
			$info = 'Strength';
			$error = 0;
			$legendUsername = ' ';
			$legendMail = ' ';
			$legendTelegram = ' ';
			$legendImg = ' ';
			$legendPass = ' ';
			$legendTweeter = ' ';
			$encontradoTelegram = false;
			$chat_id = 'null';

			$colorUsername=' ';
			$colorMail = ' ' ;
			$colorPass = ' ';
			$colorTweeter = ' ';


			$this->assign('info',$info);

			$submit = Filter::getString( 'submit' );
			if ($submit === 'Enviar') {
				//Comprobamos el nombre
				$nombre = Filter::getString( 'username' );
				if( (strlen($nombre)) < 6 ){
					//ERROR
					$legendUsername = "Como mínimo debe contener 6 carácteres.";
					$error=1;
					$colorUsername = 'red';

					for ($i=0; $i<strlen($nombre); $i++){
						if($nombre[$i]==' '){
							if($error==1){
								$error=12;
								$legendUsername = "Como mínimo debe contener 6 carácteres y no puede contener espacios.";
								$colorUsername = 'red';
							}else{
								$error=2;
								$colorUsername = 'red';
								$legendUsername = "No puede contener espacios.";
							}
						}
					}
				}else{
					for ($i=0; $i<strlen($nombre); $i++){
						if($nombre[$i]==' '){
							$error=2;
							$legendUsername = "No puede contener espacios.";
							$colorUsername = 'red';
						}
					}
				}
				$obj = $this->getClass('HomeDatabaseModel');
				$data = $obj->existe('username',$nombre);
				if($data == false){
				}else{
					$colorUsername = 'red';
					$legendUsername = 'El username escogido ya existe, pruebe con otro.';
					$error=3;
				}
				//Comprobamos el correo
				$mail = Filter::getString( 'mail' );
				if(strlen($mail)>0){
					for ($i=0; $i<strlen($mail); $i++){
						if($mail[$i]=='@'&& $encontradoArroba==1){
							$encontradoArroba=2;
						}
						if($mail[$i]=='@'&& $encontradoArroba==0){
							$encontradoArroba=1;
						}
						if($mail[$i]=='.'&& $encontradoArroba==1){
							$encontradoPunto=1;
						}
					}
					if($encontradoArroba==1 && $encontradoPunto==1){
						//Correcto
					}else{
						$error=4;
						$legendMail = "El formato no es correcto.";
						$colorMail = "red";
					}
				}else{
					$legendMail = "El correo es obligatorio.";
					$colorMail = "red";
				}
				$obj = $this->getClass('HomeDatabaseModel');
				$data = $obj->existe('email',$mail);
				if($data == false){
				}else{
					$colorMail = 'red';
					$legendMail = 'El mail ya existe.';
					$error=4;
				}
				//Comprobamos la contraseña
				$password = Filter::getString( 'password' );
				if(strlen($password)>=6 && strlen($password)<=10){
					for($i=0; $i<strlen($password); $i++){
						if(ctype_upper ( $password[$i])){
							$encontradoMayuscula = 1;
						}
						if(ctype_lower($password[$i])){
							$encontradoMinuscula=1;
						}
						if(ctype_digit($password[$i])){
							$encontradoNumero=1;
						}
					}
					if($encontradoMayuscula != 1 || $encontradoMinuscula != 1  ||  $encontradoNumero != 1){
						$error=1;
						$info = "Weak!";
						$color = 'red';
						$colorPass = "red";
						$legendPass = "La contraseña no es suficientemente segura. Debe tener minúsculas, mayúsculas y números.";
					}
				}else{
					if(strlen($password)<6){
						$legendPass = "El tamaño no es correcto, debe tener como mínimo 6 carácteres.";
						$info = "More Characters";
						$color = 'gray';
						$colorPass = "red";
						$error=1;
					}
				}
				//Comprobamos el tweeter:
				$tweeter = Filter::getString( 'tweeter' );
				if(strlen($tweeter)>0){
					if($tweeter[0]!='@'){
						$error=1;
						$colorTweeter = "red";
						$legendTweeter = "Debe comenzar por arroba";
					}
				}

				$telegram = Filter::getString( 'telegram' );
				if(strlen($telegram)>0){
					$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/getUpdates?offset=0';
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch, CURLOPT_URL, $url);
					$result = curl_exec($ch);
					curl_close($ch);
					$obj = json_decode($result,true);
					//CAMBIAR EL CHAT ID.
					for ($i = 0; $i < sizeof($obj['result']); $i++) {
						if($encontradoTelegram==false && $obj['result'][$i]['message']['from']['username'] == $telegram){
							$encontradoTelegram = true;
							$chat_id = $obj['result'][$i]['message']['chat']['id'];
						}
					}
					if($encontradoTelegram == false){
						$error=1;
						$legendTelegram = "No has iniciado conversación con nuestro bot!";
					}
				}

				//Tratamos la imagen.
				$img = ($_FILES["fichero_usuario"]["tmp_name"]);
				$file = $_FILES['fichero_usuario']['name'];
				$file_loc = $_FILES['fichero_usuario']['tmp_name'];
				$folder="http://g6.dev/images/avatar/";
				$random = rand(0,4738473);
				$img = $folder.$nombre.$random.'-'.$file;
				$folder = "images/avatar/";

				if($file == null){
					 $img = Filter::getString( 'src_Sec' );
					if($img==null){
						$img = 'http://g6.dev/images/avatar/none.jpg';
					}
				}
				if ($img != 'http://g6.dev/images/avatar/none.jpg') {
					$tipo = pathinfo($img, PATHINFO_EXTENSION);
					if ($tipo != 'png' && $tipo != 'jpg' && $tipo != 'jpeg' && $tipo != 'gif') {
						$error = 10;
						$legendImg = "El formato solo puede ser jpg, png y gif!";
					}
				}
				if($error!= 10 && $img!='http://g6.dev/images/avatar/none.jpg'){
					move_uploaded_file($file_loc,$folder.$file);
					rename ($folder.$file, $folder.$nombre.$random.'-'.$file);
				}
				if($error==1){
				}

				if($error==0){

					if($file == null){
						$img = Filter::getString( 'src_Sec' );
						if($img!=null){
							$pathant = substr($img, 14);
							$nombreImagen = substr($img, 28);
							if($img!='http://g6.dev/images/avatar/none.jpg'){
								rename ($pathant, "images/avatar/".$nombre."-".$nombreImagen);
							}

						}else{
							$img = 'http://g6.dev/images/avatar/none.jpg';
						}
					}
					if($encontradoTelegram == true){
						$queryArray = [
							'chat_id' => $chat_id,
							'text' => 'Bienvenido a Wallapulpo',
						];

						$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?'.http_build_query($queryArray);
						file_get_contents($url);
					}

					$obj = $this->getClass('HomeDatabaseModel');
					$code = sha1(mt_rand().time().mt_rand().$_SERVER['REMOTE_ADDR']);
					while ($obj->existe('codigoActivacion',$code)) {
						$code = sha1(mt_rand().time().mt_rand().$_SERVER['REMOTE_ADDR']);
					}

					$obj = $this->getClass('HomeDatabaseModel');
					$obj->insertUser($nombre,$mail, $password, $tweeter, $img, $chat_id, $code);


					require_once('phpmailer/class.phpmailer.php');

					$mail2 = new PHPMailer();
					$mail2->CharSet =  "utf-8";
					$mail2->IsSMTP();
					$mail2->SMTPAuth = true;
					$mail2->Username = "wallapulpo@gmail.com";
					$mail2->Password = "Wallapulpo1!";
					$mail2->SMTPSecure = "ssl";
					$mail2->Host = "smtp.gmail.com";
					$mail2->Port = "465";

					$mail2->setFrom('wallapulpo@gmail.com', 'Equipo de Wallapulpo');
					$mail2->AddAddress("{$mail}", "{$nombre}");

					$mail2->Subject  =  'Activación de la cuenta de Wallapulpo';
					$mail2->IsHTML(true);
					$mail2->Body    = "http://g6.dev/activation/"."{$code}";


					if($mail2->Send())
					{
						$this->setLayout( 'register/sent.tpl' );
						$this->assign('message','http://g6.dev/images/error/mailsent.png');
					}
					else
					{
						$this->setLayout( 'register/sent.tpl' );

						//No se envia el correo
						$this->assign('message','http://g6.dev/images/error/mailnotsent.png');
					}
				}
				$this->assign('colorUsername',$colorUsername);
				$this->assign('colorMail',$colorMail);
				$this->assign('colorPass',$colorPass);
				$this->assign('colorTweeter',$colorTweeter);
				$this->assign('legendTweeter',$legendTweeter);
				$this->assign('legendPass',$legendPass);
				$this->assign('legendMail',$legendMail);
				$this->assign('legendimg',$legendImg);
				$this->assign('legendUsername',$legendUsername);
				$this->assign('legendTelegram',$legendTelegram);
				$this->assign('telegram',$telegram);
				$this->assign('color',$color);
				$this->assign('info',$info);
				$this->assign('img',$img);
				$this->assign('tweeter',$tweeter);
				$this->assign('password',$password);
				$this->assign('name',$nombre);
				$this->assign('mail',$mail);

			}
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}