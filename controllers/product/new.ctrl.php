<?php
/**
 * Home Controller: Controller example.

 */
class ProductNewController extends Controller
{
	protected $view = 'product/form.tpl';

	public function build()
	{
		//Asignamos la vista
		//$this->setLayout($this->view);
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][0])){
			$this->setLayout('error/error404.tpl');
			header("HTTP/1.1 404 Not Found");
		}else {
			$this->setLayout($this->view);
			if ($user = Session::getInstance()->get('logged')) {
				$info = 1;

				$legendNombre = ' ';
				$legendDescripcion = ' ';
				$legendPrecio = ' ';
				$legendStock = ' ';
				$legendFecha = ' ';
				$legendImg = ' ';
				$cambiarUrl = 0;

				$error = 0;

				$color = 'white';

				$colorNombre = ' ';
				$colorDescripcion = ' ';
				$colorPrecio = ' ';
				$colorStock = ' ';

				//MISMO QUE EN EDIT
				$submit = Filter::getString('submit');
				if ($submit === 'Enviar') {
					//Comprobamos el nombre
					$nombreproducto = Filter::getString('nombreproducto');
					if ((strlen($nombreproducto)) > 50) {
						//ERROR
						$legendNombre = "Como maximo debe contener 50 carácteres.";
						$error = 1;
						$colorNombre = 'red';
					} else {
						if ((strlen($nombreproducto)) < 1) {
							//ERROR
							$legendNombre = "El nombre del producto es obligatorio.";
							$error = 1;
							$colorNombre = 'red';
						}
					}

					//Comprobamos la descripcion
					$descripcion = Filter::getString('src_Desc');
					if ($descripcion == "<p><br></p>") {
						$legendDescripcion = "La descripción es obligatoria.";
						$error = 1;
						$colorDescripcion = "red";
					}

					//Comprobamos el precio
					$precio = Filter::getString('precio');
					if ($precio <= 0.0) {
						$error = 1;
						$colorPrecio = "red";
						$legendPrecio = "El precio debe ser mayor que 0!";
					}

					//Comprobamos el stock
					$stock = Filter::getString('stock');
					if ($stock < 1) {
						$error = 1;
						$colorStock = "red";
						$legendStock = "El stock debe ser como mínimo 1!!";
					} else {
						$obj = $this->getClass('HomeDatabaseModel');
						$dineroBase = $obj->saldoDisponible(Session::getInstance()->get('username'));
						if ($dineroBase[0]['dineroActual'] < $stock) {
							$error = 1;
							$colorStock = "red";
							$legendStock = "No tienes suficiente dinero en la cuenta!";
						}

					}
					$caducidad = Filter::getString('caducidad');
					$date = date('Y-m-d');
					if ($caducidad < $date) {
						$error = 1;
						$legendFecha = "La fecha de caducidad no puede ser anterior que hoy!";
					}

					$img = ($_FILES["fichero_producto"]["tmp_name"]);
					$file = $_FILES['fichero_producto']['name'];
					$file_loc = $_FILES['fichero_producto']['tmp_name'];
					$folder = "http://g6.dev/images/products/";
					$img = $folder . $file;
					$folder = "images/products/";

					if($file == null){
						$img = Filter::getString( 'src_Sec' );
						if($img==null){
							$img = 'http://g6.dev/images/products/none-400x300.jpg';
						}
					}
					if ($img != 'http://g6.dev/images/products/none-400x300.jpg') {
						if($file != null){
							$tipo = pathinfo($img, PATHINFO_EXTENSION);
							if ($tipo != 'png' && $tipo != 'jpg' && $tipo != 'jpeg' && $tipo != 'gif') {
								$error = 10;
								$legendImg = "El formato solo puede ser jpg, png y gif!";
							}
							if ($_FILES['fichero_producto']['size'] > 2000000){
								$error = 10;
								$legendImg = "Pesa demasiado!";
							}
						}
					}
					if($error!= 10 && $img!='http://g6.dev/images/products/none-400x300.jpg'){
						if($file!=null){
							$ruta_imagen = $file_loc;
							$info_imagen = getimagesize($ruta_imagen);
							$imagen_ancho = $info_imagen[0];
							$imagen_alto = $info_imagen[1];
							$imagen_tipo = $info_imagen['mime'];
							switch ( $imagen_tipo ){
								case "image/jpg":
								case "image/jpeg":
									$imagen = imagecreatefromjpeg( $ruta_imagen );
									break;
								case "image/png":
									$imagen = imagecreatefrompng( $ruta_imagen );
									break;
								case "image/gif":
									$imagen = imagecreatefromgif( $ruta_imagen );
									break;
							}

							$nombrep = str_replace(" ", "_", $nombreproducto);
							$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
								'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
								'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
								'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
								'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '`'=> '_', '´'=>'_', '^'=>'', '!'=>'','"'=>'', '·'=>'.', '$'=>'S', '%'=>'_',
								'&'=>'', '/'=>'_', '('=>'_', ')'=>'_', '='=>'_', '\''=>'_', '|'=>'_', '@'=>'a', '#'=>'_', '#'=>'_', '¢'=>'_','∞'=>'_', '¬'=>'_', '÷'=>'_',
								'“'=>'_', '”'=>'_','≠'=>'_');
							$nombrep = strtr( $nombrep, $unwanted_array );
							$file = str_replace(" ", "_", $file);
							$file = strtr( $file, $unwanted_array );
							$nombreUsuario = Session::getInstance()->get('username');
							$lienzo = imagecreatetruecolor( 100, 100 );
							imagecopyresampled($lienzo, $imagen, 0, 0, 0, 0, 100, 100, $imagen_ancho, $imagen_alto);
							imagejpeg($lienzo, $folder . $nombreUsuario . '-' . $nombrep . '-100x100-'. $file, 80);

							$lienzo = imagecreatetruecolor( 400, 300 );
							imagecopyresampled($lienzo, $imagen, 0, 0, 0, 0, 400, 300, $imagen_ancho, $imagen_alto);
							imagejpeg($lienzo, $folder . $nombreUsuario . '-' . $nombrep . '-400x300-'. $file, 80);

							$folder = "http://g6.dev/images/products/";
							$img = $folder . $nombreUsuario . '-' . $nombrep  .'-400x300-'. $file ;
						}

					}

					if ($error == 0) {
						$newUrl='';
						$nombreUsuario = Session::getInstance()->get('username');
						$passUsuario = Session::getInstance()->get('password');
						$obj = $this->getClass('HomeDatabaseModel');
						$obj->insertarSaldo(Session::getInstance()->get('username'), $dineroBase[0]['dineroActual'] - $stock);
						$url = str_replace(" ", "_", $nombreproducto);
						$unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
							'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
							'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
							'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
							'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '`'=> '_', '´'=>'_', '^'=>'', '!'=>'','"'=>'', '·'=>'.', '$'=>'S', '%'=>'_',
							'&'=>'', '/'=>'_', '('=>'_', ')'=>'_', '='=>'_', '\''=>'_', '|'=>'_', '@'=>'a', '#'=>'_', '#'=>'_', '¢'=>'_','∞'=>'_', '¬'=>'_', '÷'=>'_',
							'“'=>'_', '”'=>'_','≠'=>'_');
						$url = strtr( $url, $unwanted_array );
						$obj = $this->getClass('HomeDatabaseModel');
						$result = $obj->existeUrl($url);
						if (count($result) == 0) {
							//Se queda tal cual.
							$newUrl = $url;
						}else{
							$cambiarUrl =1;
							$incremento = 2;
							while($cambiarUrl == 1){
								$obj = $this->getClass('HomeDatabaseModel');
								$newUrl = $url.'_'.$incremento;
								$result = $obj->existeUrl($newUrl);
								if(count($result)==0){
									$cambiarUrl =0;
								}else{
									$incremento++;
								}
							}
						}
						$url = $newUrl;
						$obj->insertProduct($nombreproducto, $descripcion, $nombreUsuario, $passUsuario, $date, $caducidad, $img, $stock, $url, $precio);

						header('Location: http://g6.dev/product/'.$url);

					}



					$this->assign('nombreproducto', $nombreproducto);
					$this->assign('stock', $stock);
					$this->assign('descripcion', $descripcion);
					$this->assign('precio', $precio);
					$this->assign('caducidad', $caducidad);

					$this->assign('legendNombre', $legendNombre);
					$this->assign('legendDescripcion', $legendDescripcion);
					$this->assign('legendPrecio', $legendPrecio);
					$this->assign('legendStock', $legendStock);
					$this->assign('legendFecha', $legendFecha);
					$this->assign('legendImg', $legendImg);

					$this->assign('colorNombre', $colorNombre);
					$this->assign('colorDescripcion', $colorDescripcion);
					$this->assign('colorPrecio', $colorPrecio);
					$this->assign('colorStock', $colorStock);


					$this->assign('color', $color);
					$this->assign('info', $info);
					$this->assign('img', $img);
				}
				$this->assign('titulo','NUEVO');
			} else {
				$this->setLayout('error/error403.tpl');
				header("HTTP/1.1 403 Forbidden");
			}
		}
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}
