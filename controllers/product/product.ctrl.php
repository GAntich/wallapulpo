<?php
/**
 * Home Controller: Controller example.

 */
class ProductProductController extends Controller
{
	protected $view;

	public function build()
	{
		//Asignamos la vista
		//$this->setLayout($this->view);
		//Guardamos los parametros de la URL.
		$params = $this->getParams();

		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][1])&&($params['url_arguments'][0] != 'myProducts' && $params['url_arguments'][0] != 'suprime')){
			$obj = $this->getClass('HomeDatabaseModel');
			$result = $obj->existeUrl($params['url_arguments'][0]);
			if($obj->comprobarProducto($params['url_arguments'][0])!=false){
				$datetime1 = new DateTime($result[0]['fecha_eliminacion']);
				$datetime2 = new DateTime("today");

				$interval = $datetime2->diff($datetime1);
				$tiempo = $interval->format('%R%a dias');

				if ($tiempo[0] == '-') {
					$this->setLayout('error/error404.tpl');
					header("HTTP/1.1 404 Not Found");
				} else {
					if($params['url_arguments'][1]==='confirm'){
						$comprando=Session::getInstance()->get('comprando');
						if($comprando == 'No'){
							$this->setLayout( 'error/error404.tpl' );
							header("HTTP/1.1 404 Not Found");
						}else{
							$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
							if($telegram != false){
								$this->setLayout('product/view.tpl');
								$this->assign('telegram',true);
								$submit = Filter::getString( 'submit' );
								if($submit === 'Comprar'){
									$code = Filter::getString( 'codeTelegram' );
									$nombre = Session::getInstance()->get('username');
									$password = Session::getInstance()->get('password');
									$obj = $this->getClass('HomeDatabaseModel');
									$codeBase = $obj->getCodeTelegram($nombre,$password);
									if($code==$codeBase[0]['codigoTelegram']){
										$nombreVendedor = $obj->usernamefromid($result[0]['idusuario']);
										$correoVendedor = $obj->correoVendedor($result[0]['idusuario']);
										$dineroBase = $obj->saldoDisponible(Session::getInstance()->get('username'));
										$dineroActualizado = $dineroBase[0]['dineroActual']-$result[0]['precio'];
										$nombre = Session::getInstance()->get('username');
										$obj = $this->getClass('HomeDatabaseModel');
										$obj->insertarSaldo($nombre,$dineroActualizado);
										$saldoVendedor = $obj->saldoDisponible($nombreVendedor[0]['username']);
										$obj->insertarSaldo($nombreVendedor[0]['username'],($saldoVendedor[0]['dineroActual']+$result[0]['precio']));
										$obj->updateStock($result[0]['id'], $result[0]['stock']-1);
										//id comprador
										$idComprador = $obj->getIdUser(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
										$obj->insertarCompra($result[0]['id'], $idComprador[0]['id'], $result[0]['idusuario']);
										require_once('phpmailer/class.phpmailer.php');

										$mail2 = new PHPMailer();
										$mail2->CharSet =  "utf-8";
										$mail2->IsSMTP();
										$mail2->SMTPAuth = true;
										$mail2->Username = "wallapulpo@gmail.com";
										$mail2->Password = "Wallapulpo1!";
										$mail2->SMTPSecure = "ssl";
										$mail2->Host = "smtp.gmail.com";
										$mail2->Port = "465";

										$mail2->setFrom('wallapulpo@gmail.com', 'Equipo de Wallapulpo');

										$mail2->AddAddress("{$correoVendedor[0]['email']}", "{$nombreVendedor[0]['username']}");

										$mail2->Subject = 'Felicidades, acaba de vender uno de sus productos!';
										$content = "<p>Ha vendido el dron con nombre: ".$result[0]['nombre']."</p><p>Su saldo acual es: " .$saldoVendedor[0]['dineroActual']."€</p>";
										$mail2->Body =  $content;

										$mail2->IsHTML(true);
										if($mail2->Send())
										{
											$this->setLayout( 'product/sent.tpl' );
											$this->assign('message','http://g6.dev/images/error/mailsent.png');
										}
										else
										{
											$this->setLayout( 'product/sent.tpl' );
											//No se envia el correo
											$this->assign('message','http://g6.dev/images/error/mailsent.png');
										}

										//Cogemos el telegram del vendedor y si tiene le enviamos un mensaje informándole de la venta.

										$telegramChat = $obj->getTelegramFromId($result[0]['idusuario']);
										if($telegramChat[0]['count(*)']!=0){
											$queryArray = [
												'chat_id' => $telegramChat[0]['chatTelegram'],
												'text' => 'Felicidades, acaba de vender su producto '.$result[0]['nombre'],
											];
											$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?'.http_build_query($queryArray);
											file_get_contents($url);
										}
										$compra = 'No';
										Session::getInstance()->set('comprando', $compra);
										header('Location: http://g6.dev/purse');
									}else{
										$this->assign('legendTelegram','Este no es el código!');
									}
								}
							}else{
								$this->setLayout('error/error404.tpl');
								header("HTTP/1.1 404 Not Found");
							}
						}
					}else{
						$this->setLayout('error/error404.tpl');
						header("HTTP/1.1 404 Not Found");
					}
				}
			}else{
				$this->setLayout('error/error404.tpl');
				header("HTTP/1.1 404 Not Found");
			}
		}else if($params['url_arguments'][0] != 'myProducts'  && $params['url_arguments'][0] != 'suprime') {
			$compra = 'No';
			Session::getInstance()->set('comprando', $compra);
			$obj = $this->getClass('HomeDatabaseModel');
			$result = $obj->existeUrl($params['url_arguments'][0]);
			if (count($result) == 0) {
				$obj = $this->getClass('HomeDatabaseModel');
				$resultAnt = $obj->existeUrlAntigua($params['url_arguments'][0]);
				if (count($resultAnt) == 0) {
					$this->setLayout('error/error404.tpl');
					header("HTTP/1.1 404 Not Found");
				} else {
					header("HTTP/1.1 301 Redirect");
					header('Location: http://g6.dev/product/' . $resultAnt[0]['url']);
				}
			} else {
				$obj = $this->getClass('HomeDatabaseModel');
				if ($obj->comprobarProducto($params['url_arguments'][0]) != false) {
					$datetime1 = new DateTime($result[0]['fecha_eliminacion']);
					$datetime2 = new DateTime("today");

					$interval = $datetime2->diff($datetime1);
					$tiempo = $interval->format('%R%a dias');

					if ($tiempo[0] == '-') {
						$this->setLayout('error/error404.tpl');
						header("HTTP/1.1 404 Not Found");
					} else {

						$obj = $this->getClass('HomeDatabaseModel');
						$visitas = $obj->getVisitas($result[0]['id']);
						$visitasAct = $visitas[0]['visitas'] + 1;
						$obj->setVisitas($result[0]['id'], $visitasAct);

						$this->assign('nombre', $result[0]['nombre']);
						$this->assign('descripcion', $result[0]['descripcion']);
						if ($tiempo == '+0 dias') $tiempo = 'hoy';
						if ($tiempo == '+1 dias') $tiempo = 'mañana';
						$this->assign('tiempo', $tiempo);
						$this->assign('visitas', $visitasAct);
						$this->assign('img', $result[0]['imagen']);
						$this->assign('precio', $result[0]['precio']);
						$this->assign('stock', $result[0]['stock']);
						$obj = $this->getClass('HomeDatabaseModel');
						$nombreUsuario = $obj->usernamefromid($result[0]['idusuario']);
						$this->assign('nombreUsuario', $nombreUsuario[0]['username']);
						$obj = $this->getClass('HomeDatabaseModel');
						$id = $result[0]['idusuario'];
						$imgUsuario = $obj->imageUserfromid($id);
						$this->assign('imgUser', $imgUsuario[0]['fotoPerfil']);
						$obj = $this->getClass('HomeDatabaseModel');
						$factorExito = $obj->getFactorExito($result[0]['idusuario']);
						if ($factorExito[0]['count(*)'] < 5) {
							$this->assign('factor', 1);
						} else if ($factorExito[0]['count(*)'] < 10) {
							$this->assign('factor', 2);
						} else if ($factorExito[0]['count(*)'] < 100) {
							$this->assign('factor', 3);
						} else {
							$this->assign('factor', 4);
						}
						$ventasUser = $obj->getUserVentas($result[0]['idusuario']);
						$this->assign('ventas', $ventasUser[0]['count(*)']);
						$obj = $this->getClass('HomeDatabaseModel');
						$comentarios = $obj->getComentarios($result[0]['idusuario']);
						$this->assign('comentarios', $comentarios);
						$this->assign('logged', Session::getInstance()->get('logged'));
						$legendPrecio = ' ';

						$sub = Filter::getString('submit');
						if ($sub === 'Comprar') {
							$obj = $this->getClass('HomeDatabaseModel');
							$dineroBase = $obj->saldoDisponible(Session::getInstance()->get('username'));
							if ($dineroBase[0]['dineroActual'] < $result[0]['precio']) {
								$legendPrecio = 'No tienes suficiente dinero!';
							} else {
								$dineroActualizado = $dineroBase[0]['dineroActual'] - $result[0]['precio'];
								$nombre = Session::getInstance()->get('username');
								$obj = $this->getClass('HomeDatabaseModel');

								$nombreVendedor = $obj->usernamefromid($result[0]['idusuario']);
								$saldoVendedor = $obj->saldoDisponible($nombreVendedor[0]['username']);
								$correoVendedor = $obj->correoVendedor($result[0]['idusuario']);


								//Enviamos correo al vendedor para notificarle su venta
								require_once('phpmailer/class.phpmailer.php');

								$mail2 = new PHPMailer();
								$mail2->CharSet = "utf-8";
								$mail2->IsSMTP();
								$mail2->SMTPAuth = true;
								$mail2->Username = "wallapulpo@gmail.com";
								$mail2->Password = "Wallapulpo1!";
								$mail2->SMTPSecure = "ssl";
								$mail2->Host = "smtp.gmail.com";
								$mail2->Port = "465";

								$mail2->setFrom('wallapulpo@gmail.com', 'Equipo de Wallapulpo');

								$mail2->AddAddress("{$correoVendedor[0]['email']}", "{$nombreVendedor[0]['username']}");

								$mail2->Subject = 'Felicidades, acaba de vender uno de sus productos!';
								$content = "<p>Ha vendido el dron con nombre: " . $result[0]['nombre'] . "</p><p>Su saldo acual es: " . $saldoVendedor[0]['dineroActual'] . "€</p>";
								$mail2->Body = $content;

								$mail2->IsHTML(true);

								$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'), Session::getInstance()->get('password'));
								if ($telegram != false) {
									$codigoRandom = '';
									$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
									$max = strlen($pattern) - 1;
									for ($i = 0; $i < 8; $i++) $codigoRandom .= $pattern{mt_rand(0, $max)};
									$obj->updateCodeTelegram($codigoRandom, Session::getInstance()->get('username'), Session::getInstance()->get('password'));
									$queryArray = [
										'chat_id' => $telegram[0]['chatTelegram'],
										'text' => 'Para realizar la compra, introduce el siguiente código: ' . $codigoRandom,
									];
									$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?' . http_build_query($queryArray);
									file_get_contents($url);
									$compra = 'Si';
									Session::getInstance()->set('comprando', $compra);
									header('Location: http://g6.dev/product/' . $params['url_arguments'][0] . '/confirm');
								} else {
									$compra = 'No';
									Session::getInstance()->set('comprando', $compra);
									$obj->insertarSaldo($nombre,$dineroActualizado);
									$saldoVendedor = $obj->saldoDisponible($nombreVendedor[0]['username']);
									$obj->insertarSaldo($nombreVendedor[0]['username'], ($saldoVendedor[0]['dineroActual'] + $result[0]['precio']));
									$obj->updateStock($result[0]['id'], $result[0]['stock'] - 1);
									//id comprador
									$idComprador = $obj->getIdUser(Session::getInstance()->get('username'), Session::getInstance()->get('password'));
									$obj->insertarCompra($result[0]['id'], $idComprador[0]['id'], $result[0]['idusuario']);
									if ($mail2->Send()) {
										$this->setLayout('product/sent.tpl');
										$this->assign('message', 'http://g6.dev/images/error/mailsent.png');
									} else {
										$this->setLayout('product/sent.tpl');
										//No se envia el correo
										$this->assign('message', 'http://g6.dev/images/error/mailsent.png');
									}
									header('Location: http://g6.dev/purse');
								}
							}
							$obj = $this->getClass('HomeDatabaseModel');
							$visitas = $obj->getVisitas($result[0]['id']);
							$visitasAct = $visitas[0]['visitas'] - 1;
							$obj->setVisitas($result[0]['id'], $visitasAct);
						}
						$this->assign('legendPrecio', $legendPrecio);
						$this->setLayout('product/view.tpl');
					}
				} else {
					$this->setLayout('error/error404.tpl');
					header("HTTP/1.1 404 Not Found");
				}
			}
		}else if($params['url_arguments'][0] === 'myProducts') {
			$compra = 'No';
			Session::getInstance()->set('comprando', $compra);
			if ($user = Session::getInstance()->get('logged')) {
				$obj = $this->getClass('HomeDatabaseModel');
				$id = $obj->getIdUser(Session::getInstance()->get('username'), Session::getInstance()->get('password'));
				$list = $obj->getProductsOrdered($id[0]['id']);

				$entera = (count($list) - count($list) % 10) / 10;

				$resto = count($list) % 10;

				if ($resto == 0) $paginas = $entera;
				else            $paginas = $entera + 1;

				if ($params['url_arguments'][1] <= $paginas && $params['url_arguments'][1] > 0) {
					if ($params['url_arguments'][1] == $paginas) {
						$fin = count($list) % 10;
						if (count($list) == 10) {
							$fin = 10;
						}

					} else    $fin = 10;
					$incremento = 0;
					for ($i = ($params['url_arguments'][1] - 1) * 10; $i < ($params['url_arguments'][1] - 1) * 10 + $fin; $i++) {

						$listaAEnviar[$incremento] = $list[$i];
						$incremento++;
					}
					$this->assign('noProducts', false);
					$this->assign('productos', $listaAEnviar);
					$this->assign('paginas', $paginas);
					$this->assign('actual', $params['url_arguments'][1]);
					$this->assign('productos', $listaAEnviar);
					$this->setLayout('product/myProducts.tpl');
				} else {
					if ($params['url_arguments'][1] == 1 && count($list) == 0) {
						$this->assign('noProducts', true);
						$this->setLayout('product/myProducts.tpl');
					} else {
						$this->setLayout('error/error404.tpl');
						header("HTTP/1.1 404 Not Found");
					}

				}
			} else {
				$this->setLayout('error/error403.tpl');
				header("HTTP/1.1 403 Forbidden");
			}
		}else if($params['url_arguments'][0] === 'suprime'){
			$compra = 'No';
			Session::getInstance()->set('comprando', $compra);
			if ($user = Session::getInstance()->get('logged')) {
				$obj = $this->getClass('HomeDatabaseModel');
				if($obj->comprobarProducto($params['url_arguments'][1], Session::getInstance()->get('username'),Session::getInstance()->get('password'))==true){
					$this->setLayout('product/suprime.tpl');
					$sub = Filter::getString('submit');
					if ($sub === 'si') {
						$obj = $this->getClass('HomeDatabaseModel');
						$obj->dropProduct($params['url_arguments'][1]);
						header('Location: http://g6.dev/product/myProducts/1');
					}
					if($sub === 'no'){
						header('Location: http://g6.dev/product/myProducts/1');
					}
				}else{
					$this->setLayout('error/error404.tpl');
					header("HTTP/1.1 404 Not Found");
				}
			}else{
				$this->setLayout('error/error403.tpl');
				header("HTTP/1.1 403 Forbidden");
			}
		}else{
			$this->setLayout('error/error404.tpl');
			header("HTTP/1.1 404 Not Found");
		}
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']    = 'SharedHeadController';
		$modules['footer']  = 'SharedFooterController';
		return $modules;
	}
}