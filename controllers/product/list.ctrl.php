<?php
/**
 * Home Controller: Controller example.

 */
class ProductListController extends Controller
{
	protected $view = 'product/topProducts.tpl';

	public function build()
	{
		//Asignamos la vista
		//$this->setLayout($this->view);
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][1])&&$params['url_arguments'][0] != 'list'){
			$this->setLayout('error/error404.tpl');
			header("HTTP/1.1 404 Not Found");
		}else {
			//$obj = $this->getClass('HomeDatabaseModel');
			//$data = $obj->getMostViewed('-1');
			if(isset($params['url_arguments'][0])){

				$listaAEnviar = array();
				$obj = $this->getClass('HomeDatabaseModel');
				$list = $obj->getMostViewed('-1');

				

				$entera = (count($list) - count($list) % 10) / 10;

				$resto = count($list) %10;


				if($resto==0) 	$paginas = $entera;
				else			$paginas = $entera+1;

				if($params['url_arguments'][0] <= $paginas && $params['url_arguments'][0]>0){
					if($params['url_arguments'][0] == $paginas){
						$fin = count($list) %10;
						if( count($list) == 10){
							$fin = 10;
						}
					}
					else	$fin = 10;
					$incremento = 0;
					for ($i =($params['url_arguments'][0]-1)*10 ; $i < ($params['url_arguments'][0]-1)*10+$fin; $i++) {
						$listaAEnviar[$incremento] = $list[$i];
						$ventas = $obj->getVentas($list[$i]['id']);
						$listaAEnviar[$incremento]['totalVentas'] = $ventas[0]['count(*)'];
						if(strlen($listaAEnviar[$i]['descripcion'])>50){
							$listaAEnviar[$i]['descripcion'] = substr($listaAEnviar[0]['descripcion'], 3, 50). "...";
						}
						$incremento ++;
					}

					$visitasTotal = 0;
					for($i = 0; $i<count($list);$i++){
						$visitasTotal += $list[$i]['visitas'];
					}

					for($i = 0; $i<count($listaAEnviar);$i++){
						$porcentage[$i] = intval($listaAEnviar[$i]['visitas']*100/$visitasTotal);
					}


					$this->assign('productos', $listaAEnviar);
					$this->assign('porcentage', $porcentage);
					$this->assign('paginas', $paginas);
					$this->assign('actual',$params['url_arguments'][0]);


					$this->setLayout( $this->view );
				}else{
					if(count($list)==0 && $params['url_arguments'][0] ==1){
						$this->assign('sinProductos',true);
						$this->setLayout( $this->view );
					}else{
						$this->setLayout('error/error404.tpl');
						header("HTTP/1.1 404 Not Found");
					}
				}
			} else {
				$this->setLayout('error/error404.tpl');
				header("HTTP/1.1 404 Not Found");
			}
		}
	}

	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}
