<?php
/**
 * Home Controller: Controller example.

 */
class ForgotpasswordController extends Controller
{
	protected $view = 'forgotpassword/forgotpassword.tpl';

	public function build()
	{
		$this->setLayout( $this->view );
		$params = $this->getParams();

		if(isset($params['url_arguments'][0])&&$params['url_arguments'][0] != 'forgotpassword'){
			$this->setLayout('error/error404.tpl');
			header("HTTP/1.1 404 Not Found");
		}else {
			$submit = Filter::getString( 'submit' );
			if ($submit === 'Enviar') {
				//Comprobamos el correo
				$mail = Filter::getString( 'mail' );
				$legendMail = '';
				$colorMail = '';
				if(strlen($mail)<0) {
					$legendMail = "El correo es obligatorio.";
					$colorMail = "red";
				} else {
					$obj = $this->getClass('HomeDatabaseModel');
					$data = $obj->existe('email',$mail);
					if($data){
						$code = sha1(mt_rand().time().mt_rand().$_SERVER['REMOTE_ADDR']);
						while ($obj->existe('codigoActivacion',$code)) {
							$code = sha1(mt_rand().time().mt_rand().$_SERVER['REMOTE_ADDR']);
						}
						//enviamos mail para recuperar la contraseña
						require_once('phpmailer/class.phpmailer.php');
						$mail2 = new PHPMailer();
						$mail2->CharSet =  "utf-8";
						$mail2->IsSMTP();
						$mail2->SMTPAuth = true;
						$mail2->Username = "wallapulpo@gmail.com";
						$mail2->Password = "Wallapulpo1!";
						$mail2->SMTPSecure = "ssl";
						$mail2->Host = "smtp.gmail.com";
						$mail2->Port = "465";

						$mail2->setFrom('wallapulpo@gmail.com', 'Equipo de Wallapulpo');
						$mail2->AddAddress("{$mail}");

						$mail2->Subject  =  'Recuperación contraseña';
						$mail2->IsHTML(true);
						$mail2->Body = "http://g6.dev/retrieve/"."{$code}";

						$obj->insertarCodigoPassword($mail,$code);
						$obj->repeatPassword($mail, 0);

						if($mail2->Send()) {
							$this->setLayout( 'register/sent.tpl' );
							$this->assign('message','http://g6.dev/images/error/mailsent.png');
						} else {
							$this->setLayout( 'register/sent.tpl' );

							//No se envia el correo
							$this->assign('message','http://g6.dev/images/error/mailsent.png');

						}


					}else{
						$colorMail = 'red';
						$legendMail = 'El email no existe.';
					}
				}
				$this->assign('colorMail',$colorMail);
				$this->assign('legendMail',$legendMail);
				$this->assign('mail',$mail);
			}
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}