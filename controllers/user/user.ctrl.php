<?php
/**
 * Home Controller: Controller example.

 */
class UserController extends Controller
{
	protected $view = 'product/user.tpl';

	public function build()
	{
		$this->setLayout( $this->view );
		$params = $this->getParams();
		$error = 0;
		$this->setLayout('user/user.tpl');
		if(isset($params['url_arguments'][0])){
			if (Session::getInstance()->get('logged')) {
				$this->assign('logged',true);
				$obj = $this->getClass('HomeDatabaseModel');
				if($params['url_arguments'][0] == Session::getInstance()->get('username')){
					$id = $obj->getIdUser(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
					$miscomentarios = $obj->getComentariosMios($id[0]['id']);
					$comments = $obj->getComentarios($id[0]['id']);
					$this->assign('firstComment',false);
					$this->assign('usuario',Session::getInstance()->get('username'));
					$this->assign('miscomentarios',$miscomentarios);
					$this->assign('comentarios',$comments);
					$this->assign('miusuario',1);
					$iduser = $id[0]['id'];
					$img = $obj->imageUserfromid($iduser);
					$this->assign('imgUser',$img[0]['fotoPerfil']);
					$this->setLayout('user/user.tpl');
				}else{
					$nombreUsuario = Session::getInstance()->get('username');
					$passUsuario = Session::getInstance()->get('password');
					$myid = $obj->getIdUser($nombreUsuario,$passUsuario);
					$usuario = $obj->existeUser($params['url_arguments'][0]);
					if(count($usuario)==0){
						$this->setLayout( 'error/error404.tpl' );
						header("HTTP/1.1 404 Not Found");
					}else{
						$submit = Filter::getString('submit');
						if ($submit === 'Enviar') {
							$newcomment = Filter::getString('newcomment');
							if ((strlen($newcomment)) < 1) {
								$legendComment = "El comentario es demasiado corto.";
								$this->assign('legendComment',$legendComment);
								$error = 1;
							}
							if($error == 0) {
								$obj = $this->getClass('HomeDatabaseModel');
								$obj->insertarComentario($myid[0]['id'],$usuario[0]['id'],$newcomment,date('Y-m-d'));
							}
						}
						if($submit ==='Edit'){
							$comment = Filter::getString('editcomment');
							$obj->updateComment($myid[0]['id'], $usuario[0]['id'], $comment, date('Y-m-d'));
						}
						if($submit === 'Si'){
							$obj->dropComment($myid[0]['id'], $usuario[0]['id']);
						}
						$comments = $obj->getComentarios($usuario[0]['id']);
						for($i=0; $i<count($comments); $i++) {
							if($comments[$i]['username']== Session::getInstance()->get('username')){
								$comments[$i]['mio'] = 1;
							}else{
								$comments[$i]['mio'] = 0;
							}
						}

						$hayVentas = $obj->HayVentas($myid[0]['id'],$usuario[0]['id']);
						if($hayVentas==true){
							$hayComentarios = $obj->HayComentarios($myid[0]['id'],$usuario[0]['id']);
							if($hayComentarios == false){
								$this->assign('firstComment',true);
							}else{
								$this->assign('firstComment', false);
							}
						}else{
							$this->assign('firstComment', false);
						}
						$this->assign('comentarios',$comments);
						$this->assign('usuario',$params['url_arguments'][0]);
						$this->assign('miusuario',0);
						$this->assign('imgUser',$usuario[0]['fotoPerfil']);
					}
				}
			}else{
				$this->assign('logged',false);
			}
		}else {
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}