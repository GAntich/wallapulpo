<?php
/**
 * Home Controller: Controller example.

 */
class RetrieveUserController extends Controller
{
	protected $view = 'user/retrieve.tpl';

	public function build()
	{
		//Asignamos la vista
		$this->setLayout( $this->view );
		//Guardamos los parametros de la URL.
		$params = $this->getParams();
		//En el caso de que haya algún otro parámetro en la URL, mostramos error.
		if(isset($params['url_arguments'][1])){
			//Cambia template y indica el error con el código.
			$this->setLayout( 'error/error404.tpl' );
			header("HTTP/1.1 404 Not Found");
		}else{
			$this->assign('correct',false);
			if(isset($params['url_arguments'][0])){
				//Comprovamos que el código de activación sea correcto.
				$obj = $this->getClass('HomeDatabaseModel');
				$data = $obj->infoUsuario($params['url_arguments'][0]);
				if ($data[0]['repeatPassword'] === '1') {
					$this->assign('correct',false);
				}else{
					if($data) {
						$this->assign('correct', true);
						Session::getInstance()->set('logged', true);
						$this->assign('name', $data[0]['username']);
						$this->assign('mail', $data[0]['email']);
						$this->assign('tweeter', $data[0]['tweeter']);
						$this->assign('telegram', $data[0]['chatTelegram']);
						$this->assign('img', $data[0]['fotoPerfil']);
						$submit = Filter::getString('submit');
						if ($submit === 'Enviar') {
							$obj = $this->getClass('HomeDatabaseModel');
							$password = Filter::getString('password');
							$update = $obj->updatePassword($data[0]['id'], $password);
							if ($update) {
								$obj = $this->getClass('HomeDatabaseModel');
								$data = $obj->repeatPassword($data[0]['email'], 1);
								if ($data) {
									header("Location: http://g6.dev");
								}



							}
						}
					}
				}
			}else{
				$this->setLayout( 'error/error404.tpl' );
				header("HTTP/1.1 404 Not Found");
			}
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}