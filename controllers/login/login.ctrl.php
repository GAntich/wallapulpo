<?php
/**
 * Home Controller: Controller example.

 */
class LoginController extends Controller
{
	protected $view = 'login/login.tpl';

	public function build()
	{
		$this->setLayout( $this->view );
		$params = $this->getParams();

		if(isset($params['url_arguments'][0])){
			if($params['url_arguments'][0]=='confirm'){
				$obj = $this->getClass('HomeDatabaseModel');
				$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
				if($telegram != false){
					$this->assign('telegram',true);
					$submit = Filter::getString( 'submitHead' );
					if($submit === 'Enviar'){
						$code = Filter::getString( 'codeTelegram' );
						$nombre = Session::getInstance()->get('username');
						$password = Session::getInstance()->get('password');
						$obj = $this->getClass('HomeDatabaseModel');
						$codeBase = $obj->getCodeTelegram($nombre,$password);
						if($code==$codeBase[0]['codigoTelegram']){
							Session::getInstance()->set('logged', true);
							$this->assign('telegram',false);
							header('Location: http://g6.dev');
						}else{
							$this->assign('legendTelegram','Este no es el código!');
						}
					}
				}else{
					$this->setLayout( 'error/error404.tpl' );
					header("HTTP/1.1 404 Not Found");
				}
			}else{
				$this->setLayout( 'error/error404.tpl' );
				header("HTTP/1.1 404 Not Found");
			}
		}else{
			$this->assign('telegram',false);
			$error=0;
			$submit = Filter::getString( 'submitHead' );
			if (Session::getInstance()->get('logged')==true) {
				header('Location: http://g6.dev');
			}else{
				if($submit === 'Enviar'){
					//Comprobamos el nombre
					$nombre = Filter::getString( 'usernameHead' );
					$obj = $this->getClass('HomeDatabaseModel');
					$data = $obj->existe('username',$nombre);
					if($data==true){
						Session::getInstance()->set('username', $nombre);
						//Comprobamos la contraseña
						$password = Filter::getString( 'password' );
						$obj = $this->getClass('HomeDatabaseModel');
						$data = $obj->comprobarPassNombre($nombre,$password);
						if($data==false){
							$error=1;
						}
					}else{
						$nombre = Filter::getString( 'usernameHead' );
						Session::getInstance()->set('mail', $nombre);
						$obj = $this->getClass('HomeDatabaseModel');
						$data = $obj->existe('email',$nombre);
						if($data==true) {
							//Comprobamos la contraseña
							$password = Filter::getString('password');
							$obj = $this->getClass('HomeDatabaseModel');
							$data = $obj->comprobarPassMail($nombre, $password);
							if ($data == false) {
								$error = 1;
							}else{
								$data = $obj->usernamefrommail($nombre);
								Session::getInstance()->set('username', $data[0]['username']);
							}
						}else{
							$error=1;
						}
					}
					if($error==1){
						$legend = "Nombre de usuario/contraseña incorrecto.";
						$this->setLayout( 'login/login.tpl' );
						Session::getInstance()->delete('username');
						$this->assign('legend',$legend);
					}else{
						$obj = $this->getClass('HomeDatabaseModel');
						Session::getInstance()->set('password', $password);
						$user = Session::getInstance()->get('username');
						$telegram = $obj->getTelegramChatId(Session::getInstance()->get('username'),Session::getInstance()->get('password'));
						if($telegram != false){
							Session::getInstance()->set('logged', false);
							$codigoRandom = '';
							$pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
							$max = strlen($pattern)-1;
							for($i=0;$i < 8;$i++) $codigoRandom .= $pattern{mt_rand(0,$max)};
							$obj->updateCodeTelegram($codigoRandom, Session::getInstance()->get('username'), Session::getInstance()->get('password'));
							$queryArray = [
								'chat_id' => $telegram[0]['chatTelegram'],
								'text' => 'Para hacer login, introduce el siguiente código: '.$codigoRandom,
							];
							$url = 'https://api.telegram.org/bot180830215:AAGXXALzA6xTPHryu6LRa0zwvdJmiQQy5vY/sendMessage?'.http_build_query($queryArray);
							file_get_contents($url);
							header('Location: http://g6.dev/login/confirm');
						}else{
							if($user==null){
								Session::getInstance()->set('logged', true);
								$obj = $this->getClass('HomeDatabaseModel');
								$data = $obj->usernamefrommail($nombre);
								Session::getInstance()->set('username', $data[0]['username']);
							}
							Session::getInstance()->set('logged', true);
							header('Location: http://g6.dev');
						}
					}
				}
			}
		}
	}


	/**
	 * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
	 * The sintax is the following:
	 * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
	 *
	 * @return array
	 */
	public function loadModules() {
		$modules['head']	= 'SharedHeadController';
		$modules['footer']	= 'SharedFooterController';
		return $modules;
	}
}