<?php /* Smarty version 2.6.14, created on 2016-05-11 18:56:51
         compiled from home/home.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


		<!-- Page Content -->
		<div class="container">

			<div id="carousel" class="carousel slide" data-ride="carousel">
				<!-- Menu -->
				<ol class="carousel-indicators">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
					<li data-target="#carousel" data-slide-to="2"></li>
				</ol>

				<!-- Items -->
				<div class="carousel-inner">

					<div class="item active">
						<img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/slider/1.png" alt="Slide 1" />
						<div class="carousel-caption">
							<h3>Mucha variedad</h3>
							<p>Más de 100 modelos diferentes a la venta</p>
						</div>
					</div>
					<div class="item">
						<img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/slider/2.png" alt="Slide 2" />
						<div class="carousel-caption">
							<h3>Wallapulpo</h3>
							<p>Lider de ventas de drones de segunda mano.</p>
						</div>
					</div>
					<div class="item">
						<img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/slider/3.png" alt="Slide 3" />
						<div class="carousel-caption">
							<h3>Para tus necesidades</h3>
							<p>Encuentra el dron que se adapte a ti.</p>
						</div>
					</div>
				</div>
				<a href="#carousel" class="left carousel-control" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a href="#carousel" class="right carousel-control" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>


			<div class="row">
				<!-- Titulo de la seccion-->
				<div class="col-lg-12">
					<h3>Último producto introducido.</h3>
				</div>
					<div class="col-sm-6 col-lg-6 col-md-6">
				<?php if ($this->_tpl_vars['lastProductFlag']): ?>

						<div class="thumbnail thumbnail_big">
							<img src="<?php echo $this->_tpl_vars['img']; ?>
" alt="">
							<div class="caption_big">
								<h4 class="pull-right"><?php echo $this->_tpl_vars['precio']; ?>
 €</h4>
								<h4><a href="<?php echo $this->_tpl_vars['link']; ?>
"><?php echo $this->_tpl_vars['nombre']; ?>
</a>	</h4>
							</div>
						</div>

				<?php else: ?>
					<p>No hay productos en el sistema.</p>
				<?php endif; ?>
				</div>
			</div>


			<div class="row">
				<div class="col-lg-12">
					<h3>Los 5 productos más visionados.</h3>
				</div>
				<?php if ($this->_tpl_vars['mostViewedFlag']): ?>
					<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['mostViewed']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
						<div class="col-sm-6">
							<div class="thumbnail thumbnail_big">
								<img src="<?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['imagen']; ?>
" alt="">
								<div class="caption">
									<h4 class="pull-right"><?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['precio']; ?>
 €</h4>
									<h4><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/product/<?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['url']; ?>
"><?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['nombre']; ?>
</a>
									</h4>
									<p><?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['descripcion']; ?>
</p>
								</div>
								<div class="ratings">
									<p>Caduca el: <?php echo $this->_tpl_vars['mostViewed'][$this->_sections['i']['index']]['fecha_eliminacion']; ?>
<p>
								</div>
							</div>
						</div>
					<?php endfor; endif; ?>
				<?php else: ?>
					<div class="col-lg-12">
						<p>Todavia no hay productos en el sistema.</p>
					</div>
				<?php endif; ?>
			</div>




			<div class="row">
				<div class="col-lg-12">
					<h3>Últimas 5 imagenes.</h3>
				</div>
				<?php if ($this->_tpl_vars['lastImagesFlag']): ?>
					<?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['lastImages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
						<div class="col-sm-3 col-lg-3 col-md-3">
							<div class="thumbnail">
								<img src="<?php echo $this->_tpl_vars['lastImages'][$this->_sections['c']['index']]['imagen']; ?>
" alt="">
							</div>
						</div>
						<?php endfor; endif; ?>
					<?php else: ?>
						<div class="col-lg-12">
							<p>Todavia no hay productos en el sistema.</p>
						</div>
					<?php endif; ?>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<h3>Twitter.</h3>
				</div>
			</div>
			<div class="timeline-Widget">
				<div class="timeline-Body">
					<div class="timeline-Viewport">
						<ol class="timeline-TweetList">
							<?php $_from = $this->_tpl_vars['twits']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['curr']):
?>
								<li class="timeline-TweetList-tweet">
									<div class="timeline-Tweet">
										<div class="timeline-Tweet-brand u-floatRight">
											<div class="Icon Icon--twitter" aria-label="" title="" role="presentation"></div>
										</div>

										<div class="timeline-Tweet-author">
											<div class="TweetAuthor">
												<a class="TweetAuthor-link Identity u-linkBlend" aria-label="<?php echo $this->_tpl_vars['curr']['name']; ?>
 (screen name: <?php echo $this->_tpl_vars['curr']['screen_name']; ?>
)">
													<span class="TweetAuthor-avatar Identity-avatar">
													  <img class="Avatar" data-scribe="element:avatar" src="<?php echo $this->_tpl_vars['curr']['profile_image_url']; ?>
">
													</span>
													<span class="TweetAuthor-name Identity-name customisable-highlight" title="US Dept of Interior" data-scribe="element:name"><?php echo $this->_tpl_vars['curr']['name']; ?>
</span>
													<span class="TweetAuthor-screenName Identity-screenName" title="&lrm;@Interior" data-scribe="element:screen_name">&lrm;@<?php echo $this->_tpl_vars['curr']['screen_name']; ?>
</span>
												</a>
											</div>
										</div>

										<p class="timeline-Tweet-text" lang="es" dir="ltr"><?php echo $this->_tpl_vars['curr']['text']; ?>
</p>


									</div>
								</li>
							<?php endforeach; endif; unset($_from); ?>
						</ol>
					</div>
				</div>
			</div>
		</div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>