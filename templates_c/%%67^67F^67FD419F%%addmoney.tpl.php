<?php /* Smarty version 2.6.14, created on 2016-04-13 19:47:28
         compiled from addmoney/addmoney.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>



<!-- Page Content -->

    <div class="center-block">
        <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> RECARGA EN WALLAPULPO</h2>
    </div>

    <form method="post" class="form-horizontal">

        <div class="form-group">
            <label for="importe" class="col-sm-3 control-label" id="importe" style="color:<?php echo $this->_tpl_vars['colorUsername']; ?>
;">Importe</label>
            <div class="col-sm-8">
                <input type="number" class="form-control" name="importe" id = "importe" min="1" max="100" required>
            </div>
        </div>

        <div class="form-group">
            <label for="metodo" class="col-sm-3 control-label" id="metodo" style="color:<?php echo $this->_tpl_vars['colorMail']; ?>
;">Metodo</label>
            <div class="col-sm-9">
                <input type="radio" name="metodo" value="visa"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/visa.png" height="40" width="60">
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="paypal"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/paypal.png" height="40" width="60">
             </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="transferencia"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/transferencia.jpg" height="40" width="60">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit" onclick = "validacion();">
            </div>
        </div>

    </form>
    <!--<script>
        alert(<?php echo $this->_tpl_vars['legendImporte']; ?>
);
    </script>
    -->



<?php echo $this->_tpl_vars['modules']['footer']; ?>


