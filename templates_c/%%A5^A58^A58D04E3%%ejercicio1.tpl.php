<?php /* Smarty version 2.6.14, created on 2016-03-02 17:43:21
         compiled from ejercicio1/ejercicio1.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<!-- This is an HTML comment -->


<header id="headereje">
	<div class="site-logo" name="top"><a>Ejercicio 1</a>
		<a>Galeria de instrumentos musicales</a>
	</div>
</header>
<body>
<div class="block">
	<div class="inner-block">
		<h2><?php echo $this->_tpl_vars['h2']; ?>
</h2>
		<div>
			<img id="instrumento" src=<?php echo $this->_tpl_vars['img_url']; ?>
>
		</div>
		<div>
			<a href="<?php echo $this->_tpl_vars['prev_url']; ?>
"><img id=<?php echo $this->_tpl_vars['idizquierda']; ?>
 class="flechaizquierda" src=<?php echo $this->_tpl_vars['flechaizquierda']; ?>
></a>
			<a href="<?php echo $this->_tpl_vars['next_url']; ?>
"><img id=<?php echo $this->_tpl_vars['idderecha']; ?>
 class="flechaderecha" src=<?php echo $this->_tpl_vars['flechaderecha']; ?>
></a>
		</div>
		<div class="piefoto">
			<p><?php echo $this->_tpl_vars['nombreinstrumento']; ?>
</p>
		</div>

	</div>
</div>
</body>



<div class="clear"></div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>