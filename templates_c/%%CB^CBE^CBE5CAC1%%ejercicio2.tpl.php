<?php /* Smarty version 2.6.14, created on 2016-03-10 23:35:29
         compiled from ejercicio2/ejercicio2.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'ejercicio2/ejercicio2.tpl', 39, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<!-- This is an HTML comment -->

<header id="headereje">
	<div class="site-logo" name="top"><a>Ejercicio 2</a></div>
</header>
<body>

<form method="post">
	<div class="block">
		<div class="inner-block">
			<h2><?php echo $this->_tpl_vars['legend']; ?>
</h2>
			<div class="formulario">
					<label>Nombre instrumento: <input class="nombre" type="string" name="nombre" value="<?php echo $this->_tpl_vars['name']; ?>
"/></label>

			</div>
			<div class="formulario">
				<label>Tipo instrumento:
					<input class="tipo" list="tipo_instrumento" name="list"/>
					<datalist id="tipo_instrumento">
						<option label="Viento" value="Viento">
						<option label="Cuerda" value="Cuerda">
						<option label="Otros" value="Otros">
					</datalist>
				</label>
			</div>
			<div class="formulario">
				<label>URL: <input class="url" type="url" name="url" value="<?php echo $this->_tpl_vars['dirurl']; ?>
"/></label>
			</div>
			<input class="botonenviar" type="submit" value="Enviar" name="submit">
		</div>
	</div>
</form>
<table class="table">
	<tr>
		<th>
			Instrumentos(<?php echo count($this->_tpl_vars['instrumentosOrdenados']); ?>
)
		</th>
	</tr>
	<?php $_from = $this->_tpl_vars['instrumentosOrdenados']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
		<tr class = "<?php echo $this->_tpl_vars['v']['tipo']; ?>
">
			<td>
				<a href=Ejercicio1/<?php echo $this->_tpl_vars['v']['id_instrumento']; ?>
><?php echo $this->_tpl_vars['v']['nombre']; ?>
</a>
			</td>
			<td>
				<a href=http://g6.dev/Ejercicio2/Edit/<?php echo $this->_tpl_vars['v']['id_instrumento']; ?>
>EDITAR</a>
			</td>
			<td>
				<a href=http://g6.dev/Ejercicio2/Delete/<?php echo $this->_tpl_vars['v']['id_instrumento']; ?>
>BORRAR</a>
			</td>
		</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
<table id="leyenda">
	<tr>
		<th>
			Tipo
		</th>
		<th>
			Color
		</th>
	</tr>
	<tr>
		<td>
			Cuerda
		</td>
		<td id="Cuerda">
		</td>
	</tr>
	<tr>
		<td>
			Viento
		</td>
		<td id="Viento">
		</td>
	</tr>
	<tr>
		<td>
			Otros
		</td>
		<td id="Otros">
		</td>
	</tr>
</table>
</body>


<div class="clear"></div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>