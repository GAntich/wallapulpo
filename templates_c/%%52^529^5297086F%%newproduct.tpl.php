<?php /* Smarty version 2.6.14, created on 2016-04-14 19:09:05
         compiled from newproduct/newproduct.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>



<!-- Page Content -->
<div class="col-sm-12 col-lg-12 col-md-12">

    <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
        <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> NUEVO PRODUCTO EN WALLAPULPO</h2>


        <div class="form-group">
            <label for="nombreproducto" class="col-sm-2 control-label" id="nombreproducto" style="color:<?php echo $this->_tpl_vars['colorUsername']; ?>
;">Nombre</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nombreproducto" id="nombreproducto" maxlength="50" value="<?php echo $this->_tpl_vars['nombreproducto']; ?>
" required>
            </div>
            <div class="form-group">
                <label id="errorNombre"><?php echo $this->_tpl_vars['legendNombre']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label" id="descripcion" style="color:<?php echo $this->_tpl_vars['colorMail']; ?>
;">Descripcion</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="descripcion" name = "descripcion" value="<?php echo $this->_tpl_vars['descripcion']; ?>
" required>
            </div>
            <div class="form-group">
                <label id="errorDescripcion"><?php echo $this->_tpl_vars['legendDescripcion']; ?>
</label>
            </div>
        </div>


        <div class="form-group">
            <label for="precio" class="col-sm-2 control-label" style="color:<?php echo $this->_tpl_vars['colorPass']; ?>
;" id = "precio">Precio</label>
            <div class="col-sm-10">
                <input class="form-control" id="precio" name = "precio" type="number" min="0" value="<?php echo $this->_tpl_vars['precio']; ?>
" required>
            </div>
            <div class="form-group">
                <label id="errorPrecio"><?php echo $this->_tpl_vars['legendPrecio']; ?>
</label>
            </div>
        </div>


        <div class="form-group">
            <label for="stock" class="col-sm-2 control-label" style="color:<?php echo $this->_tpl_vars['colorPass']; ?>
;" id ="stock">Stock</label>
            <div class="col-sm-10">
                <input class="form-control" id="stock" name = "stock" type="number" min="0" value="<?php echo $this->_tpl_vars['stock']; ?>
" required>
            </div>
            <div class="form-group">
                <label id="errorStock"><?php echo $this->_tpl_vars['legendStock']; ?>
</label>
            </div>
        </div>


        <div class="form-group">
            <label for="caducidad" class="col-sm-2 control-label" style="color:<?php echo $this->_tpl_vars['colorPass']; ?>
;" id ="caducidad">Caducidad</label>
            <div class="col-sm-10">
                <input type="date" name="caducidad" min="date" value="<?php echo $this->_tpl_vars['caducidad']; ?>
" required>
            </div>
            <div class="form-group">
                <label id="errorFecha"><?php echo $this->_tpl_vars['legendFecha']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="fotoproducto" class="col-sm-2 control-label">Foto producto</label>
            <div class="col-sm-10">
                <img id="imagen" src= "<?php echo $this->_tpl_vars['img']; ?>
"/>
                <input name="fichero_producto" type="file" onchange="document.getElementById('imagen').src = window.URL.createObjectURL(this.files[0])"/>
            </div>
        </div>
        <input type="hidden" name="src_Sec" value = "<?php echo $this->_tpl_vars['img']; ?>
">

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit" onclick = "validacionProducto();">
            </div>
        </div>
    </form>
</div>



<?php echo $this->_tpl_vars['modules']['footer']; ?>