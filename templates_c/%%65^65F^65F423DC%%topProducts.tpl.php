<?php /* Smarty version 2.6.14, created on 2016-05-13 20:49:30
         compiled from product/topProducts.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

    <div class="row">
    <?php if ($this->_tpl_vars['sinProductos'] == 'Si'): ?>
        <h3>No hay productos!</h3>
    <?php else: ?>
        <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['productos']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
            <div class="col-sm-offset-1 col-sm-10">
                <div class="thumbnail thumbnail_big">
                    <img class="media-object" src="<?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['imagen']; ?>
">

                    <div class="caption-full">
                        <h4 class="media-heading"><a href="http://g6.dev/product/<?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['url']; ?>
"><?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['nombre']; ?>
</a></h4>
                        <p><?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['descripcion']; ?>
</p>
                        <p class="pull-right text-info">Fecha de eliminación: <?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['fecha_eliminacion']; ?>
</p>
                        <p class="text-info">Visitas: <?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['visitas']; ?>
</p>
                        <p class="text-info">Ventas: <?php echo $this->_tpl_vars['productos'][$this->_sections['c']['index']]['totalVentas']; ?>
</p>
                    </div>
                    <div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $this->_tpl_vars['porcentage'][$this->_sections['c']['index']]; ?>
" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $this->_tpl_vars['porcentage'][$this->_sections['c']['index']]; ?>
%;min-width: 2em;">
                                <?php echo $this->_tpl_vars['porcentage'][$this->_sections['c']['index']]; ?>
%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endfor; endif; ?>
            <nav class="text-center">
                <ul class="pagination">
                    <li <?php if ($this->_tpl_vars['actual'] == 1): ?>class="disabled"<?php endif; ?>>
                        <?php if ($this->_tpl_vars['actual'] != 1): ?><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/list/<?php echo $this->_tpl_vars['actual']-1; ?>
" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        <?php else: ?>
                            <span aria-hidden="true">&laquo;</span>
                        <?php endif; ?>
                    </li>
                    <?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)1;
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['paginas']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
$this->_sections['foo']['step'] = 1;
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>
                    <li<?php if ($this->_sections['foo']['index'] == $this->_tpl_vars['actual']): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/list/<?php echo $this->_sections['foo']['index']; ?>
"><?php echo $this->_sections['foo']['index']; ?>
</a></li>
                    <?php endfor; endif; ?>
                    <li <?php if ($this->_tpl_vars['actual'] == $this->_tpl_vars['paginas']): ?>class="disabled"<?php endif; ?>>
                        <?php if ($this->_tpl_vars['actual'] != $this->_tpl_vars['paginas']): ?>
                        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/list/<?php echo $this->_tpl_vars['actual']+1; ?>
" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        <?php else: ?>
                            <span aria-hidden="true">&raquo;</span>
                        <?php endif; ?>
                    </li>
                </ul>
            </nav>
        <?php endif; ?>
    </div>

<?php echo $this->_tpl_vars['modules']['footer']; ?>