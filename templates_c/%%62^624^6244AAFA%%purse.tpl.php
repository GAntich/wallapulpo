<?php /* Smarty version 2.6.14, created on 2016-05-10 17:07:06
         compiled from purse/purse.tpl */ ?>

<?php echo $this->_tpl_vars['modules']['head']; ?>

<?php if ($this->_tpl_vars['telegram']): ?>
<form method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="password">Código de telegram</label>
        <div class="col-sm-8">
            <input type="text" name="codeTelegram" class="form-control" id="codeTelegram" maxlength="10" placeholder="código de telegram" required>
            <label id="errorTelegram" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendTelegram']; ?>
</label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-8">
            <input class="btn btn-default" type="submit" value="Enviar" name="submit">
        </div>
    </div>
</form>
<?php else: ?>
    <!-- Page Content -->
    <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/scripts.js"></script>

    <div class="center-block">
        <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> RECARGA EN WALLAPULPO</h2>
    </div>
    <form method="post" class="form-horizontal" onsubmit = "return validacionPurse();">

        <div class="form-group">
            <label for="importe" class="col-sm-3 control-label" id="importeLabel" style="color:<?php echo $this->_tpl_vars['colorUsername']; ?>
;">Importe</label>
            <div class="col-sm-8">
                <input type="Number" step="0.01"  class="form-control" name="importe" id = "importe">
                <label for="importe" class="col-sm-12 text-danger" id="errorImporte"><?php echo $this->_tpl_vars['legendImporte']; ?>
 <?php echo $this->_tpl_vars['legendError']; ?>
</label>
            </div>

        </div>

        <div class="form-group">
            <label for="metodo" class="col-sm-3 control-label" id="metodo" style="color:<?php echo $this->_tpl_vars['colorMail']; ?>
;">Metodo</label>
            <div class="col-sm-9">
                <input type="radio" name="metodo" value="visa"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/visa.png" height="40" width="60">
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="paypal"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/paypal.png" height="40" width="60">
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="transferencia"><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/paymethods/transferencia.jpg" height="40" width="60">
                <label for="metodo" class="col-sm-12 text-danger" id="errorMetodo"><?php echo $this->_tpl_vars['legendMetodo']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
            </div>
        </div>

    </form>

    <div class="row">
        <div class="col-sm-offset-1 col-lg-10">
            <h3>Búsqueda de productos.</h3>
        </div>

        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
            <div class="col-sm-9 col-sm-offset-1">
                <input class="form-control" id="busqueda" name ="busqueda" type="text" placeholder="Buscar producto...">
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <input class="btn btn-default" type="submit" value="Buscar" name="submit">
                </div>
            </div>
        </form>
        <div class="col-sm-offset-1 col-lg-10">
            <h3>Historial de compras.</h3>
        </div>
        <div class="col-sm-offset-1 col-sm-10">
            <?php if ($this->_tpl_vars['flagCompras']): ?>
                <table class="table table-hover">
                    <tr>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Usuario</th>
                    </tr>

                    <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['activos']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
                        <tr>
                            <td><a href="http://g6.dev/product/<?php echo $this->_tpl_vars['activos'][$this->_sections['c']['index']]['url']; ?>
"> <?php echo $this->_tpl_vars['activos'][$this->_sections['c']['index']]['nombre']; ?>
</a></td>
                            <td><?php echo $this->_tpl_vars['activos'][$this->_sections['c']['index']]['precio']; ?>
</td>
                            <td><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/user/<?php echo $this->_tpl_vars['activos'][$this->_sections['c']['index']]['idusuario']; ?>
"><?php echo $this->_tpl_vars['activos'][$this->_sections['c']['index']]['idusuario']; ?>
</a></td>
                        </tr>
                    <?php endfor; endif; ?>
                    <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['inactivos']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
                        <tr>
                            <td><p class="text-muted"><?php echo $this->_tpl_vars['inactivos'][$this->_sections['c']['index']]['nombre']; ?>
</p></td>
                            <td><?php echo $this->_tpl_vars['inactivos'][$this->_sections['c']['index']]['precio']; ?>
</td>
                            <td><a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/user/<?php echo $this->_tpl_vars['inactivos'][$this->_sections['c']['index']]['idusuario']; ?>
"><?php echo $this->_tpl_vars['inactivos'][$this->_sections['c']['index']]['idusuario']; ?>
</a></td>
                        </tr>
                    <?php endfor; endif; ?>
                </table>
            <?php else: ?>
                <p>No tienes ninguna compra registrada.</p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>





<?php echo $this->_tpl_vars['modules']['footer']; ?>


