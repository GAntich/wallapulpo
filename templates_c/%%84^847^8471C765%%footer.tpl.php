<?php /* Smarty version 2.6.14, created on 2016-05-11 11:42:08
         compiled from shared/footer.tpl */ ?>
		</div>
		<!-- /.container -->
		<div class="clear spacer"></div>

		<footer class="navbar-fixed-bottom">
			<div class="container">
				<p class="text-muted">© 2016 GSA Inc. Guillermo Antich, Silvia Cano, Alba Llauró</p>
			</div>
		</footer>
		<script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/jquery.min.js"></script>
		<script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/bootstrap.min.js"></script>
		<script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/summernote.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

		<script>
			<?php echo '
				$(document).ready(function() {
					$(\'#summernote\').summernote();
					$(\'#summernote-new\').summernote();
					$(\'#datetimepicker\').datetimepicker({
						format: \'YYYY-MM-DD\'
					});
					$(\'#editcomment\').hide();
					$(\'#edit\').click(function(){
						$(\'#deletecomment\').hide();
						$(\'#comentarioInicial\').hide();
						$(\'#edit\').hide();
						$(\'#delete\').hide();
						$(\'#editcomment\').show();

					});
					$(\'#deletecomment\').hide();
					$(\'#delete\').click(function(){
						$(\'#comentarioInicial\').hide();
						$(\'#editcomment\').hide();
						$(\'#deletecomment\').show();
						$(\'#edit\').hide();
						$(\'#delete\').hide();
					});
					$(\'#No\').click(function(){
						$(\'#comentarioInicial\').show();
						$(\'#editcomment\').hide();
						$(\'#deletecomment\').hide();
					});
					$(\'.clickable-row\').click(function() {
						window.document.location = $(this).data("href");
					});
				});
			'; ?>

		</script>

</body>
</html>