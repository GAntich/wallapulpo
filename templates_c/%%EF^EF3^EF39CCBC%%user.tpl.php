<?php /* Smarty version 2.6.14, created on 2016-05-12 00:14:39
         compiled from user/user.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'user/user.tpl', 9, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<?php if ($this->_tpl_vars['logged']): ?>
    <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/scripts.js"></script>
    <div class="col-sm-8">

        <div class="well">

            <div class="text-right">
                <h3>Comentarios sobre <?php echo ((is_array($_tmp=$this->_tpl_vars['usuario'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h3>
            </div>
            <hr>
            <?php if ($this->_tpl_vars['firstComment']): ?>
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                            <h4>Deja un comentario a <?php echo ((is_array($_tmp=$this->_tpl_vars['usuario'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h4>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" name="newcomment" id="newcomment" required></textarea>
                            </div>
                            <div class="form-group">
                                <label id="errorComment" class="text-danger"><?php echo $this->_tpl_vars['legendComment']; ?>
</label>
                            </div>
                            <div class="form-group pull-right">
                                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
            <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['comentarios']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <span class="pull-right"><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['fecha']; ?>
</span>
                        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/user/<?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['username']; ?>
"><h4><?php echo ((is_array($_tmp=$this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['username'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h4></a>
                        <?php if ($this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['mio']): ?>
                            <p id="comentarioInicial"><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['comentario']; ?>
</p>
                            <div id="editcomment">
                                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                                    <h4>Edita tu comentario a <?php echo ((is_array($_tmp=$this->_tpl_vars['usuario'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="editcomment" id="editcomment" required><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['comentario']; ?>
</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label id="errorComment" class="text-danger"><?php echo $this->_tpl_vars['legendComment']; ?>
</label>
                                    </div>
                                    <div class="form-group pull-right">
                                        <input class="btn btn-default" type="submit" value="Edit" name="submit">
                                    </div>
                                </form>
                            </div>
                            <div id="deletecomment">
                                <p>Vas a eliminar el comentario. Estás seguro?</p>
                                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                                    <div class="form-group">
                                        <input class="btn btn-success col-sm-offset-1" type="submit" value="Si" name="submit">
                                        <input class="btn btn-danger col-sm-offset-1" id = "No" type="submit" value="No" name="submit">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <p id="edit" class="pull-right"><span class="glyphicon glyphicon-pencil"></span> Editar</p>
                            </div>
                            <p>&nbsp;</p>
                            <div>
                                <p id="delete" class="pull-right"><span class="glyphicon glyphicon-trash" ></span>  Borrar</p>
                            </div>


                        <?php else: ?>
                            <p><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['comentario']; ?>
</p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endfor; endif; ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
            <div class="media">
                <div class="media-left">
                    <img id="imagenUsuario" class="media-object" src="<?php echo $this->_tpl_vars['imgUser']; ?>
">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $this->_tpl_vars['usuario']; ?>
</h4>
                    <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/mail/send/<?php echo $this->_tpl_vars['usuario']; ?>
"><span class="glyphicon glyphicon-envelope"></span> Enviar mensaje</a>
                </div>
            </div>
        </div>
    </div>

    <?php if ($this->_tpl_vars['miusuario'] == 1): ?>
        <div class="col-sm-8">

            <div class="well">

                <div class="text-right">
                    <h3>Comentarios de <?php echo ((is_array($_tmp=$this->_tpl_vars['usuario'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h3>
                </div>

                <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['miscomentarios']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="pull-right"><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['fecha']; ?>
</span>
                            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/user/<?php echo $this->_tpl_vars['miscomentarios'][$this->_sections['c']['index']]['username']; ?>
"><h4><?php echo ((is_array($_tmp=$this->_tpl_vars['miscomentarios'][$this->_sections['c']['index']]['username'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h4></a>
                            <!--<img id="imagen" src="<?php echo $this->_tpl_vars['miscomentarios'][$this->_sections['c']['index']]['fotoPerfil']; ?>
">-->

                            <p><?php echo $this->_tpl_vars['miscomentarios'][$this->_sections['c']['index']]['comentario']; ?>
</p>
                        </div>
                    </div>
                <?php endfor; endif; ?>
            </div>
        </div>
    <?php endif; ?>
<?php else: ?>
    <div class="col-sm-12">
        <p>No estás registrado / logeado. <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/register">Registrate</a> o <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/login">Logeate</a></p>
    </div>
<?php endif; ?>
<?php echo $this->_tpl_vars['modules']['footer']; ?>