<?php /* Smarty version 2.6.14, created on 2016-05-11 18:37:38
         compiled from mail/mail.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'mail/mail.tpl', 29, false),array('modifier', 'capitalize', 'mail/mail.tpl', 103, false),)), $this); ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

    <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/scripts.js"></script>
    <?php if ($this->_tpl_vars['sublayout'] == 'default'): ?>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sent">Enviados</a></li>
        <li><a data-toggle="tab" href="#received">Recibidos</a></li>
    </ul>
    <div class="tab-content">
        <div id="sent" class="tab-pane fade in active">
            <table class="table table-striped table-hover col-sm-12 mail">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Para</th>
                        <th>Mensaje</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                <?php if (! $this->_tpl_vars['sent']): ?>
                    <tr>
                        <td colspan="4" class="alert-warning">No tienes mensajes en esta bandeja.</td>
                    </tr>
                <?php else: ?>
                <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['sent']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <tr class="clickable-row<?php if ($this->_tpl_vars['sent'][$this->_sections['i']['index']]['unread']): ?> info<?php endif; ?>" data-href='<?php echo $this->_tpl_vars['url']['global']; ?>
/mail/<?php echo $this->_tpl_vars['sent'][$this->_sections['i']['index']]['conversation']; ?>
'>
                        <th class="col-sm-3"><?php echo $this->_tpl_vars['sent'][$this->_sections['i']['index']]['title']; ?>
</th>
                        <td class="col-sm-2"><?php echo $this->_tpl_vars['sent'][$this->_sections['i']['index']]['user2']; ?>
</td>
                        <td class="col-sm-5"><?php echo ((is_array($_tmp=$this->_tpl_vars['sent'][$this->_sections['i']['index']]['message'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 80) : smarty_modifier_truncate($_tmp, 80)); ?>
</td>
                        <td class="col-sm-2"><?php echo $this->_tpl_vars['sent'][$this->_sections['i']['index']]['timestamp']; ?>
</td>
                    </tr>
                <?php endfor; endif; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div id="received" class="tab-pane fade">
            <table class="table table-striped table-hover col-sm-12 mail">
                <thead>
                <tr>
                    <th>Título</th>
                    <th>De</th>
                    <th>Mensaje</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>
                <?php if (! $this->_tpl_vars['received']): ?>
                <tr>
                    <td colspan="4" class="alert-warning">No tienes mensajes en esta bandeja.</td>
                </tr>
                <?php else: ?>
                <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['received']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <tr class="clickable-row<?php if ($this->_tpl_vars['received'][$this->_sections['i']['index']]['unread']): ?> info<?php endif; ?>" data-href='<?php echo $this->_tpl_vars['url']['global']; ?>
/mail/<?php echo $this->_tpl_vars['received'][$this->_sections['i']['index']]['conversation']; ?>
'>
                        <th class="col-sm-3"><?php echo $this->_tpl_vars['received'][$this->_sections['i']['index']]['title']; ?>
</th>
                        <td class="col-sm-2"><?php echo $this->_tpl_vars['received'][$this->_sections['i']['index']]['user1']; ?>
</td>
                        <td class="col-sm-5"><?php echo ((is_array($_tmp=$this->_tpl_vars['received'][$this->_sections['i']['index']]['message'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 80) : smarty_modifier_truncate($_tmp, 80)); ?>
</td>
                        <td class="col-sm-2"><?php echo $this->_tpl_vars['received'][$this->_sections['i']['index']]['timestamp']; ?>
</td>
                    </tr>
                <?php endfor; endif; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php elseif ($this->_tpl_vars['sublayout'] == 'view'): ?>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/mail"><span class="btn btn-primary">Bandeja de entrada</span></a>
        <h3>Escribir mensaje</h3>
                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionMensaje();">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div id="summernote"></div>
                            <input type="hidden" id="messagehidden" name="src_Msg" value ="">
                            <label id="errorMessage" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendMensaje']; ?>
</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-9 col-sm-2">
                            <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                        </div>
                    </div>
                </form>
        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['conversation']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <?php if ($this->_tpl_vars['conversation'][$this->_sections['i']['index']]['user1'] == $this->_tpl_vars['me']): ?>
            <div class="panel panel-success">
                <div class="panel-heading text-right"><span class="pull-left"><?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['timestamp']; ?>
</span><h4><?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['user1']; ?>
</h4></div>
                <div class="panel-body text-right">
                    <?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['message']; ?>

                </div>
            </div>
            <?php else: ?>
            <div class="panel panel-info">
                <div class="panel-heading"><span class="pull-right"><?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['timestamp']; ?>
</span><h4><?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['user1']; ?>
</h4></div>
                <div class="panel-body">
                <?php echo $this->_tpl_vars['conversation'][$this->_sections['i']['index']]['message']; ?>

                </div>
            </div>
            <?php endif; ?>

        <?php endfor; endif; ?>
    <?php elseif ($this->_tpl_vars['sublayout'] == 'send'): ?>
        <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/mail"><span class="btn btn-primary">Bandeja de entrada</span></a>
        <h3>Escribir mensaje para <?php echo ((is_array($_tmp=$this->_tpl_vars['to'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</h3>
        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionConversacion();">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label" id="titlelabel">Titulo:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="title" id="title" required>
                    <label id="errorTitleNew" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendTitle']; ?>
</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <div id="summernote-new"></div>
                    <input type="hidden" id="messagenewhidden" name="src_New" value ="">
                    <label id="errorMessageNew" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendMensaje']; ?>
</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-9 col-sm-2">
                    <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                </div>
            </div>
        </form>
    <?php endif; ?>

<?php echo $this->_tpl_vars['modules']['footer']; ?>