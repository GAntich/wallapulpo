<?php /* Smarty version 2.6.14, created on 2016-05-10 18:01:38
         compiled from login/login.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<?php if ($this->_tpl_vars['telegram']): ?>
<form method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="password">Código de telegram</label>
        <div class="col-sm-8">
            <input type="text" name="codeTelegram" class="form-control" id="codeTelegram" maxlength="10" placeholder="código de telegram" required>
            <label id="errorTelegram" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendTelegram']; ?>
</label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-8">
            <input class="btn btn-default" type="submit" value="Enviar" name="submitHead">
        </div>
    </div>
</form>
<?php else: ?>
    <div class="center-block">
        <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> LOGIN WALLAPULPO</h2>
    </div>

        <form class="form-horizontal" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">

        <div class="form-group">
            <label class="col-sm-3 control-label" for="usernameHead">Username</label>
            <div class="col-sm-8">
                <input type="text" name="usernameHead" class="form-control" id="usernameHead" placeholder="Login" required>
                <label id="errorUsername" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legend']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" for="password">Contraseña</label>
            <div class="col-sm-8">
                <input type="password" name="password" class="form-control" id="password" maxlength="10" placeholder="Password" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-2">
                <input class="btn btn-default" type="submit" value="Enviar" name="submitHead">
            </div>
            <div class="col-sm-offset-1 col-sm-3">
                <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/forgotpassword">Olvidaste la contraseña ?</a></div>
            </div>
    </form>
<?php endif; ?>


<?php echo $this->_tpl_vars['modules']['footer']; ?>