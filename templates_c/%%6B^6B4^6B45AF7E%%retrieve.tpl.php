<?php /* Smarty version 2.6.14, created on 2016-05-11 22:39:30
         compiled from user/retrieve.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>


<div class="row">
    <?php if ($this->_tpl_vars['correct']): ?>
        <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/scripts.js"></script>
        <div class="center-block">
            <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> RESTAURAR CONTRASEÑA EN WALLAPULPO</h2>
        </div>
        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacion();">

            <div class="form-group">
                <label for="username" class="col-sm-3 control-label" id="usernamelabel" style="color:<?php echo $this->_tpl_vars['colorUsername']; ?>
;">Username</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="username" id="nombre" value="<?php echo $this->_tpl_vars['name']; ?>
" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="mail" class="col-sm-3 control-label" id="maillabel" style="color:<?php echo $this->_tpl_vars['colorMail']; ?>
;">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" id="mail" value="<?php echo $this->_tpl_vars['mail']; ?>
" name = "mail" readonly>
                </div>

            </div>

            <div class="form-group">
                <label for="password" class="col-sm-3 control-label" style="color:<?php echo $this->_tpl_vars['colorPass']; ?>
;" id = "passwordlabel">Password<sup>*</sup></label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <input class="form-control" id="pass" value="<?php echo $this->_tpl_vars['password']; ?>
" name = "password" type="password" size="6" maxlength="10" onkeyup="passwordChanged()" required/>
                        <span class="input-group-addon" id="strength" style="background-color:<?php echo $this->_tpl_vars['color']; ?>
;color:#000"><?php echo $this->_tpl_vars['info']; ?>
</span>
                    </div>
                    <label id="errorPass" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendPass']; ?>
</label>
                </div>
            </div>

            <div class="form-group">
                <label for="tweeter" class="col-sm-3 control-label" id="tweeterlabel" name="tweeter" style="color:<?php echo $this->_tpl_vars['colorTweeter']; ?>
;">Tweeter</label>
                <div class="col-sm-8">
                    <input class="form-control" name="tweeter" id = "tweeter" type="string" value="<?php echo $this->_tpl_vars['tweeter']; ?>
" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="telegram" class="col-sm-3 control-label" id="tweeterlabel" name="telegram" style="color:<?php echo $this->_tpl_vars['colorTweeter']; ?>
;">Telegram Alias. You need to start conversation with @Wallapulpo</label>
                <div class="col-sm-8">
                    <input class="form-control" name="telegram" id = "telegram" type="string" value="<?php echo $this->_tpl_vars['telegram']; ?>
" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="fotoperfil" class="col-sm-3 control-label">Foto de perfil</label>
                <div class="col-sm-6">
                    <img id="imagen" src="<?php echo $this->_tpl_vars['img']; ?>
" class="img-thumbnail thumbnail"/>
                </div>

                <input type="hidden" name="src_Sec" value = "<?php echo $this->_tpl_vars['img']; ?>
">
            </div>

            <div class="form-group">
                <div class="col-sm-offset-6 col-sm-8">
                    <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                </div>
            </div>
        </form>
    <?php else: ?>
        <img class="message col-sm-5 col-sm-offset-2" src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/error/repeticionlinkPassword.png" >
    <?php endif; ?>

</div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>