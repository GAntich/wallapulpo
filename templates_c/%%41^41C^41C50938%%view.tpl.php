<?php /* Smarty version 2.6.14, created on 2016-05-11 23:47:12
         compiled from product/view.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

    <?php if ($this->_tpl_vars['telegram']): ?>
        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="password">Código de telegram</label>
                <div class="col-sm-8">
                    <input type="text" name="codeTelegram" class="form-control" id="codeTelegram" maxlength="10" placeholder="código de telegram" required>
                    <label id="errorTelegram" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendTelegram']; ?>
</label>
                </div>
            </div>
            <div class="form-group">
                <div class="pull-right">
                    <input class="btn btn-primary" type="submit" value="Comprar" name="submit">
                </div>
            </div>
        </form>
    <?php else: ?>
        <div class="row">
            <div class="col-sm-8">
                <div class="thumbnail thumbnail_big">
                    <img  id="imagen" src="<?php echo $this->_tpl_vars['img']; ?>
">
                    <div class="caption-full">
                        <h4 class="pull-right"><?php echo $this->_tpl_vars['precio']; ?>
  €</h4>
                        <h4><?php echo $this->_tpl_vars['nombre']; ?>
</h4>
                        <p><?php echo $this->_tpl_vars['descripcion']; ?>
</p>
                    </div>
                    <?php if ($this->_tpl_vars['logged'] == true): ?>
                        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                            <div class="form-group">
                                <div class="pull-right">
                                    <input class="btn btn-primary" type="submit" value="Comprar" name="submit">
                                </div>
                            </div>
                        </form>
                    <?php endif; ?>
                    <div class="ratings">
                        <p class="pull-right">Stock: <?php echo $this->_tpl_vars['stock']; ?>
</p>
                        <p>Caduca en: <?php echo $this->_tpl_vars['tiempo']; ?>
</p>
                        <p class = "pull-right">Visitas: <?php echo $this->_tpl_vars['visitas']; ?>
</p>
                        <p><?php echo $this->_tpl_vars['legendPrecio']; ?>
&nbsp;</p>
                    </div>
                </div>
                <div class="well">

                    <div class="text-right">
                        Comentarios sobre el vendedor
                    </div>

                    <?php unset($this->_sections['c']);
$this->_sections['c']['name'] = 'c';
$this->_sections['c']['loop'] = is_array($_loop=$this->_tpl_vars['comentarios']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['c']['show'] = true;
$this->_sections['c']['max'] = $this->_sections['c']['loop'];
$this->_sections['c']['step'] = 1;
$this->_sections['c']['start'] = $this->_sections['c']['step'] > 0 ? 0 : $this->_sections['c']['loop']-1;
if ($this->_sections['c']['show']) {
    $this->_sections['c']['total'] = $this->_sections['c']['loop'];
    if ($this->_sections['c']['total'] == 0)
        $this->_sections['c']['show'] = false;
} else
    $this->_sections['c']['total'] = 0;
if ($this->_sections['c']['show']):

            for ($this->_sections['c']['index'] = $this->_sections['c']['start'], $this->_sections['c']['iteration'] = 1;
                 $this->_sections['c']['iteration'] <= $this->_sections['c']['total'];
                 $this->_sections['c']['index'] += $this->_sections['c']['step'], $this->_sections['c']['iteration']++):
$this->_sections['c']['rownum'] = $this->_sections['c']['iteration'];
$this->_sections['c']['index_prev'] = $this->_sections['c']['index'] - $this->_sections['c']['step'];
$this->_sections['c']['index_next'] = $this->_sections['c']['index'] + $this->_sections['c']['step'];
$this->_sections['c']['first']      = ($this->_sections['c']['iteration'] == 1);
$this->_sections['c']['last']       = ($this->_sections['c']['iteration'] == $this->_sections['c']['total']);
?>
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['username']; ?>

                                <span class="pull-right"><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['fecha']; ?>
</span>
                                <p><?php echo $this->_tpl_vars['comentarios'][$this->_sections['c']['index']]['comentario']; ?>
</p>
                            </div>
                        </div>
                    <?php endfor; endif; ?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="media">
                        <div class="media-left">
                            <img id="imagenUsuario" class="media-object" src="<?php echo $this->_tpl_vars['imgUser']; ?>
">
                        </div>
                        <div class="media-body">
                            <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/user/<?php echo $this->_tpl_vars['nombreUsuario']; ?>
"><h4 class="media-heading"><?php echo $this->_tpl_vars['nombreUsuario']; ?>
</h4></a>
                            <p>
                                <?php if ($this->_tpl_vars['factor'] == 1): ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php elseif ($this->_tpl_vars['factor'] == 2): ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php elseif ($this->_tpl_vars['factor'] == 3): ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php else: ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                <?php endif; ?>
                                <?php echo $this->_tpl_vars['factor']; ?>
 Estrellas</p>
                            <p>
                                Ventas totales: <?php echo $this->_tpl_vars['ventas']; ?>

                            </p>
                            <?php if ($this->_tpl_vars['logged']): ?>
                                <a href="<?php echo $this->_tpl_vars['url']['global']; ?>
/mail/send/<?php echo $this->_tpl_vars['nombreUsuario']; ?>
"><span class="glyphicon glyphicon-envelope"></span> Enviar mensaje</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php echo $this->_tpl_vars['modules']['footer']; ?>