<?php /* Smarty version 2.6.14, created on 2016-05-10 14:48:21
         compiled from register/activation.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>

<div class="row">
    <?php if ($this->_tpl_vars['activation']): ?>
    <h4>Tu cuenta ha sido activada.</h4>
    <?php elseif ($this->_tpl_vars['errorcode']): ?>
    <h4>Ha habido un error al activar la cuenta.</h4>
    <?php elseif ($this->_tpl_vars['registered']): ?>
        <img class="message col-sm-5 col-sm-offset-2" src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/error/repeticionlink.png" >
    <?php endif; ?>
</div>
<?php echo $this->_tpl_vars['modules']['footer']; ?>