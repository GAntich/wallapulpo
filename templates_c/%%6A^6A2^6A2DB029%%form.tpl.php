<?php /* Smarty version 2.6.14, created on 2016-05-13 22:33:12
         compiled from product/form.tpl */ ?>
<?php echo $this->_tpl_vars['modules']['head']; ?>



<!-- Page Content -->
    <script src="<?php echo $this->_tpl_vars['url']['global']; ?>
/js/scripts.js"></script>
    <div class="center-block">
        <h2><img src="<?php echo $this->_tpl_vars['url']['global']; ?>
/images/logo.png" alt="" width = "40px" height="40px"> <?php echo $this->_tpl_vars['titulo']; ?>
 PRODUCTO EN WALLAPULPO</h2>
    </div>
    <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionProducto();">

        <div class="form-group">
            <label for="nombreproducto" class="col-sm-3 control-label" id="nombreproductolabel" ">Nombre</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="nombreproducto" id="nombreproducto" value="<?php echo $this->_tpl_vars['nombreproducto']; ?>
" required>
                <label id="errorNombre" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendNombre']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="descripcion" class="col-sm-3 control-label" id="descripcion">Descripcion</label>
            <div class="col-sm-8">
                <div id="summernote"><?php echo $this->_tpl_vars['descripcion']; ?>
</div>
                <input type="hidden" id="descripcionhidden" name="src_Desc" value ="">
                <label id="errorDescripcion" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendDescripcion']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="precio" class="col-sm-3 control-label" id = "precioLabel">Precio</label>
            <div class="col-sm-8">
                <input class="form-control" id="precio" name = "precio" type="Number" step="0.01" min="0" value="<?php echo $this->_tpl_vars['precio']; ?>
" required>
                <label id="errorPrecio" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendPrecio']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="stock" class="col-sm-3 control-label" id ="stockLabel">Stock</label>
            <div class="col-sm-8">
                <input class="form-control" id="stock" name = "stock" type="number" min="0" value="<?php echo $this->_tpl_vars['stock']; ?>
" required>
                <label id="errorStock" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendStock']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="caducidad" class="col-sm-3 control-label" id ="caducidad">Caducidad</label>
            <div class="col-sm-8">
                <div class='input-group date' id='datetimepicker'>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type='text' name="caducidad" class="form-control" min="date" value="<?php echo $this->_tpl_vars['caducidad']; ?>
" required />
                </div>
                <label id="errorFecha" class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendFecha']; ?>
</label>
            </div>
        </div>

        <div class="form-group">
            <label for="fotoproducto" class="col-sm-3 control-label">Foto producto</label>
            <div class="col-sm-2">
                <div class="file-upload btn btn-primary col-sm-12">
                    <span class="glyphicon glyphicon-folder-open"></span><span> Browse</span>
                    <input class="upload" name="fichero_producto" id="uploadBtn" type="file" onchange="document.getElementById('imagen').src = window.URL.createObjectURL(this.files[0]);">
                </div>
                <label id="errorImagen"  class="col-sm-12 text-danger"><?php echo $this->_tpl_vars['legendImg']; ?>
</label>
            </div>
            <div class="col-sm-6">
                <img id="imagen" src= "<?php echo $this->_tpl_vars['img']; ?>
" class="img-thumbnail thumbnail"/>
            </div>

            <input type="hidden" name="src_Sec" value = "<?php echo $this->_tpl_vars['img']; ?>
">
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
            </div>
        </div>
    </form>
    <div class="spacer-form">&nbsp;</div>
    <div class="spacer-form">&nbsp;</div>


<?php echo $this->_tpl_vars['modules']['footer']; ?>