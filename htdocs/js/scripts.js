function passwordChanged() {
    var strength = document.getElementById('strength');
    var strongRegex = new RegExp("^(?=.{6,10}$)(?=.*[A-Z])(?=.*[|!@#Â·Â¢$âˆž%Â¬&Ã·/â€œ(â€)â‰ =])(?=.*[a-z])(?=.*[0-9]).*", "g");
    var mediumRegex = new RegExp("^(?=.{6,10}$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*", "g");
    var enoughRegex = new RegExp("(?=.{6,10}).*", "g");
    var pwd = document.getElementById("pass");
    if (pwd.value.length==0) {
        strength.innerHTML = 'Type Password';
        strength.style.backgroundColor = "#5bc0de";
        strength.style.color = "#fff";
    } else if (false == enoughRegex.test(pwd.value)) {
        strength.innerHTML = 'More Characters';
        strength.style.backgroundColor = "gray";
        strength.style.color = "#fff";
    } else if (strongRegex.test(pwd.value)) {
        strength.innerHTML = 'Strong!';
        strength.style.backgroundColor = "green";
        strength.style.color = "#fff";
    } else if (mediumRegex.test(pwd.value)) {
        strength.innerHTML = 'Medium!';
        strength.style.backgroundColor = "orange";
        strength.style.color = "#fff";
    } else {
        strength.innerHTML = 'Weak!';
        strength.style.backgroundColor = "red";
        strength.style.color = "#fff";
    }
}


function validacion() {
    nombre=document.getElementById("nombre");
    var encontradoespacio = 0;
    var error = 0;
    //Comprobamos el nombre
    //Que no esté vacio.
    if( nombre == null || nombre.length == 0 || /^\s+$/.test(nombre) ) {
        document.getElementById("usernamelabel").style.color = "red";
        document.getElementById("errorUsername").innerHTML = "Es obligatorio!";
        error=1;
    }else{
        //Que tenga la longitud mínima.
        if(nombre.value.length<6){
            document.getElementById("usernamelabel").style.color = "red";
            document.getElementById("errorUsername").innerHTML = "Debe tener como mínimo 6 carácteres.";
            error=1;
            for(var i =0; i<nombre.value.length; i++){
                if(encontradoespacio== 0){
                    if(nombre.value.charAt(i)== ' '){
                        encontradoespacio = 1;
                    }
                }
            }
            if(encontradoespacio != 0){
                document.getElementById("usernamelabel").style.color = "red";
                document.getElementById("errorUsername").innerHTML = "No puede contener espacios y debe tener como mínimo 6 carácteres.";
                error=1;
            }
        }else{
            //Que no haya espacios.
            for(var i =0; i<nombre.value.length; i++){
                if(encontradoespacio== 0){
                    if(nombre.value.charAt(i)== ' '){
                        encontradoespacio = 1;
                    }
                }
            }
            if(encontradoespacio != 0){
                document.getElementById("usernamelabel").style.color = "red";
                document.getElementById("errorUsername").innerHTML = "No puede contener espacios";
                error=1;
            }
        }
    }
    if(error!=1){
        document.getElementById("usernamelabel").style.color = "black";
        document.getElementById("errorUsername").innerHTML = "";
    }
    //Comprobamos el correo
    correo = document.getElementById("mail").value;
    //Que no esté vacio.
    if( correo == null || correo.length == 0 || /^\s+$/.test(correo) ) {
        document.getElementById("maillabel").style.color = "red";
        document.getElementById( "errorMail").innerHTML = "Es obligatorio";
        error=2;
    }else{
        //Que tenga el formato de correo electrónico.
        if( !(/\S+@\S+\.\S+/.test(correo)) ) {
            document.getElementById("maillabel").style.color = "red";
            error=2;
            document.getElementById( "errorMail").innerHTML = "El formato no es correcto";
        }
    }
    if(error!=2){
        document.getElementById("maillabel").style.color = "black";
        document.getElementById( "errorMail").innerHTML = "";
    }
    //Comprovamos la contraseña
    contra = document.getElementById("pass");
    if( contra == null || contra.length == 0 || /^\s+$/.test(contra) ) {
        document.getElementById("passwordlabel").style.color = "red";
        document.getElementById( "errorPass").innerHTML = "Es obligatorio";
        error=3;
    }else{
        if(contra.value.length<6){
            document.getElementById("passwordlabel").style.color = "red";
            document.getElementById( "errorPass").innerHTML = "Es demasiado corto";
            error=3;
        }
        if(contra.value.length>10){
            document.getElementById("passwordlabel").style.color = "red";
            document.getElementById( "errorPass").innerHTML = "Es demasiado largo";
            error=3;
        }


        var strongRegex = new RegExp("^(?=.{6,10}$)(?=.*[A-Z])(?=.*[|!@#Â·Â¢$âˆž%Â¬&Ã·/â€œ(â€)â‰ =])(?=.*[a-z])(?=.*[0-9]).*", "g");
        var mediumRegex = new RegExp("^(?=.{6,10}$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*", "g");
        if(strongRegex.test(contra.value)){
            strength.innerHTML = 'Strong!';
            strength.style.backgroundColor = "green";
        }else if(mediumRegex.test(contra.value)){
            strength.innerHTML = 'Medium!';
            strength.style.backgroundColor = "orange";
        }else{
            strength.innerHTML = 'Weak!';
            strength.style.backgroundColor = "red";
            document.getElementById( "errorPass").innerHTML = "Debe contener una mayúscula, una minúscula y un número.";
            document.getElementById("passwordlabel").style.color = "red";
            error=3;
        }
    }
    if(error!=3){
        document.getElementById("passwordlabel").style.color = "black";
        document.getElementById( "errorPass").innerHTML = "";
    }
    //PASSWORD CORRECTO.
    usertweeter = document.getElementById("tweeter");
    if( usertweeter.value == null || usertweeter.value.length == 0 || /^\s+$/.test(usertweeter.value) ) {

    }else{
        if(usertweeter.value.charAt(0)!='@'){
            document.getElementById("tweeterlabel").style.color = "red";
            document.getElementById("errorTweeter").innerHTML = "El primer carácter debe ser un @";
            error=1;
        }else{
            document.getElementById("tweeterlabel").style.color = "black";
            document.getElementById("errorTweeter").innerHTML = "";
        }
    }
    if(error!=0) return false;
    return true;
}


function validacionProducto() {
    nombreproducto=document.getElementById("nombreproducto");
    var error = 0;
    //Comprobamos
    if( nombreproducto == null || nombreproducto.length == 0 || /^\s+$/.test(nombreproducto) ) {
        nombreproducto.style.color = "red";
        document.getElementById("errorNombre").innerHTML = "Es obligatorio!";
        error=1;
    }else {
        if(nombreproducto.value.length >50){
            document.getElementById("errorNombre").innerHTML = "No puede tener más de 50 carácteres!";
            error=1;
        }else{
            nombreproducto.style.color = "black";
            document.getElementById("errorNombre").innerHTML = "";
        }

    }
    descripcion = $('#summernote').summernote('code');
    document.getElementById("descripcionhidden").value=descripcion;
    //Comprobamos el nombre
    //Que no esté vacio.
    if( descripcion == null || descripcion.length == 0 || /^\s+$/.test(descripcion)||descripcion ==  "<p><br></p>" ){
        //document.getElementById("descripcion").style.color = "red";
        document.getElementById("errorDescripcion").innerHTML = "Es obligatorio!";
        error=2;
    }else {
       // document.getElementById("descripcion").style.color = "black";
        if(typeof(descripcion)!= "string"){
            document.getElementById("errorDescripcion").innerHTML = "Ha de ser un string!";
        }else{
            document.getElementById("errorDescripcion").innerHTML = "";
        }

    }

    precio=document.getElementById("precio");
    //Comprobamos el nombre
    //Que no esté vacio.
    if( precio == null || precio.length == 0 || /^\s+$/.test(precio) ) {
        precio.style.color = "red";
        document.getElementById("errorPrecio").innerHTML = "Es obligatorio!";
        error=3;
    }else {
        if (precio.value <= 0.0 ){
            precio.style.color = "red";
            document.getElementById("errorPrecio").innerHTML = "Debe ser mayor que 0.";
            error=3;
        } else {
            precio.style.color = "black";
            document.getElementById("errorPrecio").innerHTML = "";
        }
    }

    stock=document.getElementById("stock");
    //Comprobamos el nombre
    //Que no esté vacio.
    if( stock == null || stock.length == 0 || /^\s+$/.test(stock) ) {
        stock.style.color = "red";
        document.getElementById("errorStock").innerHTML = "Es obligatorio!";
        error=4;
    }else {
        if (stock.value < 1 ){
            stock.style.color = "red";
            document.getElementById("errorStock").innerHTML = "Debe ser como mínimo 1.";
            error=4;
        } else {
            stock.style.color = "black";
            document.getElementById("errorStock").innerHTML = "";
        }
    }


    if(error!=0) return false;
    return true;
}

function validacionPurse(){
    error = 0;
    importe=document.getElementById("importe");
    if(importe.value==null || importe.value.length ==0){
        document.getElementById("errorImporte").innerHTML = "Es obligatorio!";
        error=1;
    }else{
        if(importe.value>100){
            document.getElementById("errorImporte").innerHTML = "No puedes transferir más de 100 € a la vez.";
           error=1;
        }
        if(importe.value<1){
            document.getElementById("errorImporte").innerHTML = "Deber transferir por lo menos 1 €.";
            if(importe.value==0){
                document.getElementById("errorImporte").innerHTML = "Debes transferir algo";
            }
            error=1;
        }
    }

    var a = 0, pagos=document.getElementsByName("metodo");
    for(i=0;i<pagos.length;i++) {
        if(pagos.item(i).checked == false) {
            a++;
        }
    }
    if(a == pagos.length) {
        document.getElementById("errorMetodo").innerHTML = "Escoge algún método de pago.";
        error=1;
    }
    if(error==1)return false;
    return true;
}


function validacionMensaje() {
    var error = false;
    mensaje = $('#summernote').summernote('code');
    document.getElementById("messagehidden").value=mensaje;
    if( mensaje == null || mensaje.length == 0 || /^\s+$/.test(mensaje)||mensaje ==  "<p><br></p>" ){
        document.getElementById("errorMessage").innerHTML = "El mensaje debe decir algo.";
        error = true;
    }else {
        document.getElementById("errorMessage").innerHTML = "";
    }
    if(error) return false;
    return true;
}

function validacionConversacion(){
    var error = false;
    title=document.getElementById("title");
    if( title == null || title.length == 0 || /^\s+$/.test(title) ) {
        document.getElementById("errorTitle").innerHTML = "El título es obligatorio.";
        error=true;
    }else {
        if(title.value.length >50){
            document.getElementById("errorTitleNew").innerHTML = "Como maximo debe tener 50 carácteres.";
            error=true;
        }else{
            document.getElementById("errorTitleNew").innerHTML = "";
        }
    }

    mensaje = $('#summernote-new').summernote('code');
    document.getElementById("messagenewhidden").value=mensaje;
    if( mensaje == null || mensaje.length == 0 || /^\s+$/.test(mensaje)||mensaje ==  "<p><br></p>" ){
        document.getElementById("errorMessageNew").innerHTML = "El mensaje debe decir algo.";
        error = true;
    }else {
        document.getElementById("errorMessageNew").innerHTML = "";
    }
    if(error) return false;
    return true;
}
