{$modules.head}
<div class="row">
    {if $activation}
    <h4>Tu cuenta ha sido activada.</h4>
    {elseif $errorcode}
    <h4>Ha habido un error al activar la cuenta.</h4>
    {elseif $registered}
        <img class="message col-sm-5 col-sm-offset-2" src="{$url.global}/images/error/repeticionlink.png" >
    {/if}
</div>
{$modules.footer}