
{$modules.head}

<!-- Page Content -->
    <script src="{$url.global}/js/scripts.js"></script>
    <div class="center-block">
        <h2><img src="{$url.global}/images/logo.png" alt="" width = "40px" height="40px"> REGISTRATE EN WALLAPULPO</h2>
    </div>
    <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacion();">

        <div class="form-group">
            <label for="username" class="col-sm-3 control-label" id="usernamelabel" style="color:{$colorUsername};">Username<sup>*</sup></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="username" id = "nombre" value="{$name}" required>
                <label id="errorUsername" class="col-sm-12 text-danger">{$legendUsername}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="mail" class="col-sm-3 control-label" id="maillabel" style="color:{$colorMail};">Email<sup>*</sup></label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="mail" value="{$mail}" name = "mail" required>
                <label id="errorMail" class="col-sm-12 text-danger">{$legendMail}</label>
            </div>

        </div>

        <div class="form-group">
            <label for="password" class="col-sm-3 control-label" style="color:{$colorPass};" id = "passwordlabel">Password<sup>*</sup></label>
            <div class="col-sm-8">
                <div class="input-group">
                    <input class="form-control" id="pass" value="{$password}" name = "password" type="password" size="6" maxlength="10" onkeyup="passwordChanged()" required/>
                    <span class="input-group-addon" id="strength" style="background-color:{$color};color:#000">{$info}</span>
                </div>
                <label id="errorPass" class="col-sm-12 text-danger">{$legendPass}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="tweeter" class="col-sm-3 control-label" id="tweeterlabel" name="tweeter" style="color:{$colorTweeter};">Tweeter</label>
            <div class="col-sm-8">
                <input class="form-control" name="tweeter" id = "tweeter" type="string" value="{$tweeter}" />
                <label id="errorTweeter" class="col-sm-12 text-danger">{$legendTweeter}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="telegram" class="col-sm-3 control-label" id="tweeterlabel" name="telegram" style="color:{$colorTweeter};">Telegram Alias. You need to start conversation with @Wallapulpo</label>
            <div class="col-sm-8">
                <input class="form-control" name="telegram" id = "telegram" type="string" value="{$telegram}" />
                <label id="errorTelegram" class="col-sm-12 text-danger">{$legendTelegram}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="fotoperfil" class="col-sm-3 control-label">Foto de perfil</label>
            <div class="col-sm-2">
                <div class="file-upload btn btn-primary col-sm-12">
                    <span class="glyphicon glyphicon-folder-open"></span><span> Browse</span>
                    <input class="upload" name="fichero_usuario" id="uploadBtn" type="file" onchange="document.getElementById('imagen').src = window.URL.createObjectURL(this.files[0]);">
                </div>
            </div>
            <div class="col-sm-6">
                <img id="imagen" src="{$img}" class="img-thumbnail thumbnail"/>
                <label id="errorTelegram" class="col-sm-12 text-danger">{$legendimg}</label>
            </div>

            <input type="hidden" name="src_Sec" value = "{$img}">
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
            </div>
        </div>
    </form>
    <div class="spacer-form">&nbsp;</div>
    <div class="spacer-form">&nbsp;</div>




{$modules.footer}