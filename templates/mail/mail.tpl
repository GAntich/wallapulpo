{$modules.head}
    <script src="{$url.global}/js/scripts.js"></script>
    {if $sublayout == 'default'}
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sent">Enviados</a></li>
        <li><a data-toggle="tab" href="#received">Recibidos</a></li>
    </ul>
    <div class="tab-content">
        <div id="sent" class="tab-pane fade in active">
            <table class="table table-striped table-hover col-sm-12 mail">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Para</th>
                        <th>Mensaje</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                {if !$sent}
                    <tr>
                        <td colspan="4" class="alert-warning">No tienes mensajes en esta bandeja.</td>
                    </tr>
                {else}
                {section name=i loop=$sent}
                    <tr class="clickable-row{if $sent[i].unread} info{/if}" data-href='{$url.global}/mail/{$sent[i].conversation}'>
                        <th class="col-sm-3">{$sent[i].title}</th>
                        <td class="col-sm-2">{$sent[i].user2}</td>
                        <td class="col-sm-5">{$sent[i].message|truncate:80}</td>
                        <td class="col-sm-2">{$sent[i].timestamp}</td>
                    </tr>
                {/section}
                {/if}
                </tbody>
            </table>
        </div>
        <div id="received" class="tab-pane fade">
            <table class="table table-striped table-hover col-sm-12 mail">
                <thead>
                <tr>
                    <th>Título</th>
                    <th>De</th>
                    <th>Mensaje</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody>
                {if !$received}
                <tr>
                    <td colspan="4" class="alert-warning">No tienes mensajes en esta bandeja.</td>
                </tr>
                {else}
                {section name=i loop=$received}
                    <tr class="clickable-row{if $received[i].unread} info{/if}" data-href='{$url.global}/mail/{$received[i].conversation}'>
                        <th class="col-sm-3">{$received[i].title}</th>
                        <td class="col-sm-2">{$received[i].user1}</td>
                        <td class="col-sm-5">{$received[i].message|truncate:80}</td>
                        <td class="col-sm-2">{$received[i].timestamp}</td>
                    </tr>
                {/section}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
    {elseif $sublayout == 'view'}
        <a href="{$url.global}/mail"><span class="btn btn-primary">Bandeja de entrada</span></a>
        <h3>Escribir mensaje</h3>
                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionMensaje();">
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div id="summernote"></div>
                            <input type="hidden" id="messagehidden" name="src_Msg" value ="">
                            <label id="errorMessage" class="col-sm-12 text-danger">{$legendMensaje}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-9 col-sm-2">
                            <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                        </div>
                    </div>
                </form>
        {section name=i loop=$conversation}
            {if $conversation[i].user1 == $me}
            <div class="panel panel-success">
                <div class="panel-heading text-right"><span class="pull-left">{$conversation[i].timestamp}</span><h4>{$conversation[i].user1}</h4></div>
                <div class="panel-body text-right">
                    {$conversation[i].message}
                </div>
            </div>
            {else}
            <div class="panel panel-info">
                <div class="panel-heading"><span class="pull-right">{$conversation[i].timestamp}</span><h4>{$conversation[i].user1}</h4></div>
                <div class="panel-body">
                {$conversation[i].message}
                </div>
            </div>
            {/if}

        {/section}
    {elseif $sublayout == 'send'}
        <a href="{$url.global}/mail"><span class="btn btn-primary">Bandeja de entrada</span></a>
        <h3>Escribir mensaje para {$to|capitalize}</h3>
        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionConversacion();">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label" id="titlelabel">Titulo:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="title" id="title" required>
                    <label id="errorTitleNew" class="col-sm-12 text-danger">{$legendTitle}</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <div id="summernote-new"></div>
                    <input type="hidden" id="messagenewhidden" name="src_New" value ="">
                    <label id="errorMessageNew" class="col-sm-12 text-danger">{$legendMensaje}</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-9 col-sm-2">
                    <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                </div>
            </div>
        </form>
    {/if}

{$modules.footer}