<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="Página de ejercicios para Proyectos Web" />
	<meta name="keywords" content="Proyectos Web" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />		
	<title>Wallapulpo</title>
	<link rel="stylesheet" href="{$url.global}/css/bootstrap.min.css">
	<link rel="stylesheet" href="{$url.global}/css/summernote.css">
	<link rel="stylesheet" href="{$url.global}/css/timeline.f7130f11be9c2719286897ede16853b9.light.ltr.css">
	<link rel="stylesheet" href="{$url.global}/css/wallapulpo-homepage.css">




</head>


<body role="document">
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{$url.global}">Wallapulpo</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="{$url.global}">Home</a></li>
						<!--<li><a href="{$url.global}/demo">Demo</a></li>-->

							<li class="dropdown">
								<a href="{$url.global}/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos <span class="caret"></span></a>
								<ul class="dropdown-menu">
									{if $logged == true}
									<li><a href="{$url.global}/new">Crear</a></li>
									<li><a href="{$url.global}/product/myProducts/1">Mis productos</a></li>
									{/if}
									<li><a href="{$url.global}/list/1">Listado</a></li>
								</ul>
							</li>

					</ul>

					<ul class="nav navbar-nav navbar-right">
				{if $logged == true}
						<li><a href="{$url.global}/purse"><span class="glyphicon glyphicon-piggy-bank"></span> Saldo: {$money} €</a></li>
						<li class="dropdown">
							<a href="{$url.global}/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> {$nombre|capitalize}  <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="http://g6.dev/user/{$nombre}"><span class="glyphicon glyphicon-info-sign"></span> Perfil</a></li>
								<li><a href="http://g6.dev/mail"><span class="glyphicon glyphicon-envelope"></span> Mensaje {$unread}</a></li>
								<li><a href="http://g6.dev/logout"><span class="glyphicon glyphicon-remove-circle"></span> Logout</a></li>
							</ul>
						</li>
				{else}
						<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Login / Registro <span class="caret"></span></a>
							<ul id="login-dp" class="dropdown-menu">
								<li>
									 <div class="row">
											<div class="col-md-12">
												Login:
												<form class="form" role="form" method="post" action="http://g6.dev/login" accept-charset="UTF-8" id="login-nav">
														<div class="form-group">
															 <label class="sr-only" for="usernameHead">Username</label>
															 <input type="text" name="usernameHead" class="form-control" id="usernameHead" placeholder="Login" required>
														</div>
														<div class="form-group">
															 <label class="sr-only" for="password">Contraseña</label>
															 <input type="password" name="password" class="form-control" id="password" maxlength="10" placeholder="Password" required>
															 <div class="help-block text-right"><a href="{$url.global}/forgotpassword">Olvidaste la contraseña ?</a></div>
														</div>
														<div class="form-group">
															 <input class="btn btn-primary btn-block" type="submit" value="Enviar" name="submitHead">
														</div>
												 </form>
											</div>
											 <div class="bottom text-center">
												 No estás registrado? <a href="{$url.global}/register"><b>Regístrate</b></a>
											 </div>
									 </div>
								</li>
							</ul>
						</li>
				{/if}
					</ul>
				</div>
			</div>
		</nav>
		<div class="container" role="main">