{$modules.head}

		<!-- Page Content -->
		<div class="container">

			<div id="carousel" class="carousel slide" data-ride="carousel">
				<!-- Menu -->
				<ol class="carousel-indicators">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
					<li data-target="#carousel" data-slide-to="2"></li>
				</ol>

				<!-- Items -->
				<div class="carousel-inner">

					<div class="item active">
						<img src="{$url.global}/images/slider/1.png" alt="Slide 1" />
						<div class="carousel-caption">
							<h3>Mucha variedad</h3>
							<p>Más de 100 modelos diferentes a la venta</p>
						</div>
					</div>
					<div class="item">
						<img src="{$url.global}/images/slider/2.png" alt="Slide 2" />
						<div class="carousel-caption">
							<h3>Wallapulpo</h3>
							<p>Lider de ventas de drones de segunda mano.</p>
						</div>
					</div>
					<div class="item">
						<img src="{$url.global}/images/slider/3.png" alt="Slide 3" />
						<div class="carousel-caption">
							<h3>Para tus necesidades</h3>
							<p>Encuentra el dron que se adapte a ti.</p>
						</div>
					</div>
				</div>
				<a href="#carousel" class="left carousel-control" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a href="#carousel" class="right carousel-control" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>


			<div class="row">
				<!-- Titulo de la seccion-->
				<div class="col-lg-12">
					<h3>Último producto introducido.</h3>
				</div>
					<div class="col-sm-6 col-lg-6 col-md-6">
				{if $lastProductFlag}

						<div class="thumbnail thumbnail_big">
							<img src="{$img}" alt="">
							<div class="caption_big">
								<h4 class="pull-right">{$precio} €</h4>
								<h4><a href="{$link}">{$nombre}</a>	</h4>
							</div>
						</div>

				{else}
					<p>No hay productos en el sistema.</p>
				{/if}
				</div>
			</div>


			<div class="row">
				<div class="col-lg-12">
					<h3>Los 5 productos más visionados.</h3>
				</div>
				{if $mostViewedFlag}
					{section name=i loop=$mostViewed}
						<div class="col-sm-6">
							<div class="thumbnail thumbnail_big">
								<img src="{$mostViewed[i].imagen}" alt="">
								<div class="caption">
									<h4 class="pull-right">{$mostViewed[i].precio} €</h4>
									<h4><a href="{$url.global}/product/{$mostViewed[i].url}">{$mostViewed[i].nombre}</a>
									</h4>
									<p>{$mostViewed[i].descripcion}</p>
								</div>
								<div class="ratings">
									<p>Caduca el: {$mostViewed[i].fecha_eliminacion}<p>
								</div>
							</div>
						</div>
					{/section}
				{else}
					<div class="col-lg-12">
						<p>Todavia no hay productos en el sistema.</p>
					</div>
				{/if}
			</div>




			<div class="row">
				<div class="col-lg-12">
					<h3>Últimas 5 imagenes.</h3>
				</div>
				{if $lastImagesFlag}
					{section name=c loop=$lastImages}
						<div class="col-sm-3 col-lg-3 col-md-3">
							<div class="thumbnail">
								<img src="{$lastImages[c].imagen}" alt="">
							</div>
						</div>
						{/section}
					{else}
						<div class="col-lg-12">
							<p>Todavia no hay productos en el sistema.</p>
						</div>
					{/if}
			</div>

			<div class="row">
				<div class="col-lg-12">
					<h3>Twitter.</h3>
				</div>
			</div>
			<div class="timeline-Widget">
				<div class="timeline-Body">
					<div class="timeline-Viewport">
						<ol class="timeline-TweetList">
							{foreach from=$twits item=curr}
								<li class="timeline-TweetList-tweet">
									<div class="timeline-Tweet">
										<div class="timeline-Tweet-brand u-floatRight">
											<div class="Icon Icon--twitter" aria-label="" title="" role="presentation"></div>
										</div>

										<div class="timeline-Tweet-author">
											<div class="TweetAuthor">
												<a class="TweetAuthor-link Identity u-linkBlend" aria-label="{$curr.name} (screen name: {$curr.screen_name})">
													<span class="TweetAuthor-avatar Identity-avatar">
													  <img class="Avatar" data-scribe="element:avatar" src="{$curr.profile_image_url}">
													</span>
													<span class="TweetAuthor-name Identity-name customisable-highlight" title="US Dept of Interior" data-scribe="element:name">{$curr.name}</span>
													<span class="TweetAuthor-screenName Identity-screenName" title="&lrm;@Interior" data-scribe="element:screen_name">&lrm;@{$curr.screen_name}</span>
												</a>
											</div>
										</div>

										<p class="timeline-Tweet-text" lang="es" dir="ltr">{$curr.text}</p>


									</div>
								</li>
							{/foreach}
						</ol>
					</div>
				</div>
			</div>
		</div>
{$modules.footer}