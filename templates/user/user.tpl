{$modules.head}
{if $logged}
    <script src="{$url.global}/js/scripts.js"></script>
    <div class="col-sm-8">

        <div class="well">

            <div class="text-right">
                <h3>Comentarios sobre {$usuario|capitalize}</h3>
            </div>
            <hr>
            {if $firstComment}
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                            <h4>Deja un comentario a {$usuario|capitalize}</h4>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" name="newcomment" id="newcomment" required></textarea>
                            </div>
                            <div class="form-group">
                                <label id="errorComment" class="text-danger">{$legendComment}</label>
                            </div>
                            <div class="form-group pull-right">
                                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            {/if}
            {section name=c loop=$comentarios}
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <span class="pull-right">{$comentarios[c].fecha}</span>
                        <a href="{$url.global}/user/{$comentarios[c].username}"><h4>{$comentarios[c].username|capitalize}</h4></a>
                        {if $comentarios[c].mio}
                            <p id="comentarioInicial">{$comentarios[c].comentario}</p>
                            <div id="editcomment">
                                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                                    <h4>Edita tu comentario a {$usuario|capitalize}</h4>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="editcomment" id="editcomment" required>{$comentarios[c].comentario}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label id="errorComment" class="text-danger">{$legendComment}</label>
                                    </div>
                                    <div class="form-group pull-right">
                                        <input class="btn btn-default" type="submit" value="Edit" name="submit">
                                    </div>
                                </form>
                            </div>
                            <div id="deletecomment">
                                <p>Vas a eliminar el comentario. Estás seguro?</p>
                                <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                                    <div class="form-group">
                                        <input class="btn btn-success col-sm-offset-1" type="submit" value="Si" name="submit">
                                        <input class="btn btn-danger col-sm-offset-1" id = "No" type="submit" value="No" name="submit">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <p id="edit" class="pull-right"><span class="glyphicon glyphicon-pencil"></span> Editar</p>
                            </div>
                            <p>&nbsp;</p>
                            <div>
                                <p id="delete" class="pull-right"><span class="glyphicon glyphicon-trash" ></span>  Borrar</p>
                            </div>


                        {else}
                            <p>{$comentarios[c].comentario}</p>
                        {/if}
                    </div>
                </div>
            {/section}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="thumbnail">
            <div class="media">
                <div class="media-left">
                    <img id="imagenUsuario" class="media-object" src="{$imgUser}">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{$usuario}</h4>
                    <a href="{$url.global}/mail/send/{$usuario}"><span class="glyphicon glyphicon-envelope"></span> Enviar mensaje</a>
                </div>
            </div>
        </div>
    </div>

    {if $miusuario== 1}
        <div class="col-sm-8">

            <div class="well">

                <div class="text-right">
                    <h3>Comentarios de {$usuario|capitalize}</h3>
                </div>

                {section name=c loop=$miscomentarios}
                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="pull-right">{$comentarios[c].fecha}</span>
                            <a href="{$url.global}/user/{$miscomentarios[c].username}"><h4>{$miscomentarios[c].username|capitalize}</h4></a>
                            <!--<img id="imagen" src="{$miscomentarios[c].fotoPerfil}">-->

                            <p>{$miscomentarios[c].comentario}</p>
                        </div>
                    </div>
                {/section}
            </div>
        </div>
    {/if}
{else}
    <div class="col-sm-12">
        <p>No estás registrado / logeado. <a href="{$url.global}/register">Registrate</a> o <a href="{$url.global}/login">Logeate</a></p>
    </div>
{/if}
{$modules.footer}