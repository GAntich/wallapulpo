{$modules.head}
<div class="row">
    {if $noProducts}
        <h3>No hay productos!</h3>
    </div>
    {else}
        {section name=c loop=$productos}


            <div class="col-sm-8">
                <div class="thumbnail">
                    <img  id="imagen" src="{$productos[c].imagen}">
                    <div class="caption-full">
                        <h4 class="pull-right">{$productos[c].precio} €</h4>
                        <h4><a href="{$url.global}/product/{$productos[c].url}">{$productos[c].nombre}</a></h4>
                        <p>{$productos[c].descripcion}</p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">Stock: {$productos[c].stock}</p>
                        <p>Creado el: {$productos[c].fecha_creacion}<p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">Visitas: {$productos[c].visitas}</p>
                        <p>Caduca el: {$productos[c].fecha_eliminacion}<p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <p>Que deseas hacer con éste producto:</p>
                    <p class="text-center"><a class="btn btn-success active" role="button" href="http://g6.dev/edit/{$productos[c].url}">Editar</a> <a class="btn btn-danger active" role="button" href="http://g6.dev/product/suprime/{$productos[c].url}">Eliminar</a></p>
                </div>
            </div>
        {/section}
    </div>

        <nav class="text-center">
            <ul class="pagination">
                <li {if $actual == 1}class="disabled"{/if}>
                    {if $actual != 1}<a href="{$url.global}/product/myProducts/{$actual-1}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    {else}
                        <span aria-hidden="true">&laquo;</span>
                    {/if}
                </li>
                {section name=foo start=1 loop=$paginas+1}
                    <li{if $smarty.section.foo.index == $actual} class="active"{/if}><a href="{$url.global}/product/myProducts/{$smarty.section.foo.index}">{$smarty.section.foo.index}</a></li>
                {/section}
                <li {if $actual == $paginas}class="disabled"{/if}>
                    {if $actual != $paginas}
                        <a href="{$url.global}/product/myProducts/{$actual+1}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    {else}
                        <span aria-hidden="true">&raquo;</span>
                    {/if}
                </li>
            </ul>
        </nav>
    {/if}

{$modules.footer}