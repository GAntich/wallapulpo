{$modules.head}


<!-- Page Content -->
    <script src="{$url.global}/js/scripts.js"></script>
    <div class="center-block">
        <h2><img src="{$url.global}/images/logo.png" alt="" width = "40px" height="40px"> {$titulo} PRODUCTO EN WALLAPULPO</h2>
    </div>
    <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionProducto();">

        <div class="form-group">
            <label for="nombreproducto" class="col-sm-3 control-label" id="nombreproductolabel" ">Nombre</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="nombreproducto" id="nombreproducto" value="{$nombreproducto}" required>
                <label id="errorNombre" class="col-sm-12 text-danger">{$legendNombre}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="descripcion" class="col-sm-3 control-label" id="descripcion">Descripcion</label>
            <div class="col-sm-8">
                <div id="summernote">{$descripcion}</div>
                <input type="hidden" id="descripcionhidden" name="src_Desc" value ="">
                <label id="errorDescripcion" class="col-sm-12 text-danger">{$legendDescripcion}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="precio" class="col-sm-3 control-label" id = "precioLabel">Precio</label>
            <div class="col-sm-8">
                <input class="form-control" id="precio" name = "precio" type="Number" step="0.01" min="0" value="{$precio}" required>
                <label id="errorPrecio" class="col-sm-12 text-danger">{$legendPrecio}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="stock" class="col-sm-3 control-label" id ="stockLabel">Stock</label>
            <div class="col-sm-8">
                <input class="form-control" id="stock" name = "stock" type="number" min="0" value="{$stock}" required>
                <label id="errorStock" class="col-sm-12 text-danger">{$legendStock}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="caducidad" class="col-sm-3 control-label" id ="caducidad">Caducidad</label>
            <div class="col-sm-8">
                <div class='input-group date' id='datetimepicker'>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    <input type='text' name="caducidad" class="form-control" min="date" value="{$caducidad}" required />
                </div>
                <label id="errorFecha" class="col-sm-12 text-danger">{$legendFecha}</label>
            </div>
        </div>

        <div class="form-group">
            <label for="fotoproducto" class="col-sm-3 control-label">Foto producto</label>
            <div class="col-sm-2">
                <div class="file-upload btn btn-primary col-sm-12">
                    <span class="glyphicon glyphicon-folder-open"></span><span> Browse</span>
                    <input class="upload" name="fichero_producto" id="uploadBtn" type="file" onchange="document.getElementById('imagen').src = window.URL.createObjectURL(this.files[0]);">
                </div>
                <label id="errorImagen"  class="col-sm-12 text-danger">{$legendImg}</label>
            </div>
            <div class="col-sm-6">
                <img id="imagen" src= "{$img}" class="img-thumbnail thumbnail"/>
            </div>

            <input type="hidden" name="src_Sec" value = "{$img}">
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
            </div>
        </div>
    </form>
    <div class="spacer-form">&nbsp;</div>
    <div class="spacer-form">&nbsp;</div>


{$modules.footer}