{$modules.head}
    <div class="row">
    {if $sinProductos == 'Si'}
        <h3>No hay productos!</h3>
    {else}
        {section name=c loop=$productos}
            <div class="col-sm-offset-1 col-sm-10">
                <div class="thumbnail thumbnail_big">
                    <img class="media-object" src="{$productos[c].imagen}">

                    <div class="caption-full">
                        <h4 class="media-heading"><a href="http://g6.dev/product/{$productos[c].url}">{$productos[c].nombre}</a></h4>
                        <p>{$productos[c].descripcion}</p>
                        <p class="pull-right text-info">Fecha de eliminación: {$productos[c].fecha_eliminacion}</p>
                        <p class="text-info">Visitas: {$productos[c].visitas}</p>
                        <p class="text-info">Ventas: {$productos[c].totalVentas}</p>
                    </div>
                    <div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{$porcentage[c]}" aria-valuemin="0" aria-valuemax="100" style="width:{$porcentage[c]}%;min-width: 2em;">
                                {$porcentage[c]}%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/section}
            <nav class="text-center">
                <ul class="pagination">
                    <li {if $actual == 1}class="disabled"{/if}>
                        {if $actual != 1}<a href="{$url.global}/list/{$actual-1}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        {else}
                            <span aria-hidden="true">&laquo;</span>
                        {/if}
                    </li>
                    {section name=foo start=1 loop=$paginas+1}
                    <li{if $smarty.section.foo.index == $actual} class="active"{/if}><a href="{$url.global}/list/{$smarty.section.foo.index}">{$smarty.section.foo.index}</a></li>
                    {/section}
                    <li {if $actual == $paginas}class="disabled"{/if}>
                        {if $actual != $paginas}
                        <a href="{$url.global}/list/{$actual+1}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        {else}
                            <span aria-hidden="true">&raquo;</span>
                        {/if}
                    </li>
                </ul>
            </nav>
        {/if}
    </div>

{$modules.footer}