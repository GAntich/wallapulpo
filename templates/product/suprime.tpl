{$modules.head}

    <h3 class="col-sm-offset-2 col-sm-5">Seguro que quieres eliminar el producto?</h3>

    <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacionProducto();">
        <div class="form-group">
            <div class="col-sm-5 col-sm-offset-4">
                <input class="btn btn-default btn-success" type="submit" value="si" name="submit">
                <input class="btn btn-default btn-danger" type="submit" value="no" name="submit">
            </div>
        </div>
    </form>
{$modules.footer}