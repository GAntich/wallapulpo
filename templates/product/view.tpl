{$modules.head}
    {if $telegram}
        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="password">Código de telegram</label>
                <div class="col-sm-8">
                    <input type="text" name="codeTelegram" class="form-control" id="codeTelegram" maxlength="10" placeholder="código de telegram" required>
                    <label id="errorTelegram" class="col-sm-12 text-danger">{$legendTelegram}</label>
                </div>
            </div>
            <div class="form-group">
                <div class="pull-right">
                    <input class="btn btn-primary" type="submit" value="Comprar" name="submit">
                </div>
            </div>
        </form>
    {else}
        <div class="row">
            <div class="col-sm-8">
                <div class="thumbnail thumbnail_big">
                    <img  id="imagen" src="{$img}">
                    <div class="caption-full">
                        <h4 class="pull-right">{$precio}  €</h4>
                        <h4>{$nombre}</h4>
                        <p>{$descripcion}</p>
                    </div>
                    {if $logged == true}
                        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
                            <div class="form-group">
                                <div class="pull-right">
                                    <input class="btn btn-primary" type="submit" value="Comprar" name="submit">
                                </div>
                            </div>
                        </form>
                    {/if}
                    <div class="ratings">
                        <p class="pull-right">Stock: {$stock}</p>
                        <p>Caduca en: {$tiempo}</p>
                        <p class = "pull-right">Visitas: {$visitas}</p>
                        <p>{$legendPrecio}&nbsp;</p>
                    </div>
                </div>
                <div class="well">

                    <div class="text-right">
                        Comentarios sobre el vendedor
                    </div>

                    {section name=c loop=$comentarios}
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                {$comentarios[c].username}
                                <span class="pull-right">{$comentarios[c].fecha}</span>
                                <p>{$comentarios[c].comentario}</p>
                            </div>
                        </div>
                    {/section}
                </div>
            </div>

            <div class="col-sm-4">
                <div class="thumbnail">
                    <div class="media">
                        <div class="media-left">
                            <img id="imagenUsuario" class="media-object" src="{$imgUser}">
                        </div>
                        <div class="media-body">
                            <a href="{$url.global}/user/{$nombreUsuario}"><h4 class="media-heading">{$nombreUsuario}</h4></a>
                            <p>
                                {if $factor == 1}
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                {elseif $factor == 2}
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                {elseif $factor == 3}
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                {else}
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                {/if}
                                {$factor} Estrellas</p>
                            <p>
                                Ventas totales: {$ventas}
                            </p>
                            {if $logged}
                                <a href="{$url.global}/mail/send/{$nombreUsuario}"><span class="glyphicon glyphicon-envelope"></span> Enviar mensaje</a>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}
{$modules.footer}