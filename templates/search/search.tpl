{$modules.head}

{if $notfound}
<div class="row">
        <h4>No hay resultados.</h4>
</div>
{else}
{section name=c loop=$productos}

    <div class="row">
        <div class="col-sm-8">
            <div class="thumbnail thumbnail_big">
                <img  id="imagen" src="{$productos[c].imagen}">
                <div class="caption-full">
                    <a href="http://g6.dev/product/{$productos[c].url}"><h4>{$productos[c].nombre}</h4></a>
                    <p>{$productos[c].descripcion}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="thumbnail">
                <div class="media">
                    <div class="media-left">
                        <img id="imagenUsuario" class="media-object" src="{$productos[c].fotoPerfil}">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">{$productos[c].username}</h4>
                        <p>
                            {if $productos[c].factor == 1}
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            {elseif $productos[c].factor == 2}
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            {elseif $productos[c].factor == 3}
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            {else}
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                            {/if}
                            {$productos[c].factor} Estrellas</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

{/section}
    <nav class="text-center">
        <ul class="pagination">
            <li {if $actual == 1}class="disabled"{/if}>
                {if $actual != 1}<a href="{$url.global}/search/{$search}/{$actual-1}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                {else}
                    <span aria-hidden="true">&laquo;</span>
                {/if}
            </li>
            {section name=foo start=1 loop=$paginas+1}
                <li{if $smarty.section.foo.index == $actual} class="active"{/if}><a href="{$url.global}/search/{$search}/{$smarty.section.foo.index}">{$smarty.section.foo.index}</a></li>
            {/section}
            <li {if $actual == $paginas}class="disabled"{/if}>
                {if $actual != $paginas}
                    <a href="{$url.global}/search/{$search}/{$actual+1}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                {else}
                    <span aria-hidden="true">&raquo;</span>
                {/if}
            </li>
        </ul>
    </nav>
{/if}
{$modules.footer}