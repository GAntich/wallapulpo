{$modules.head}

<div class="center-block">
    <h2><img src="{$url.global}/images/logo.png" alt="" width = "40px" height="40px"> RECUPERA TU CONTRASEÑA</h2>
</div>
<form method="post" class="form-horizontal" ENCTYPE="multipart/form-data" onsubmit="return validacion();">

    <div class="form-group">
        <label for="mail" class="col-sm-3 control-label" id="maillabel" style="color:{$colorMail};">Email<sup>*</sup></label>
        <div class="col-sm-8">
            <input type="email" class="form-control" id="mail" value="{$mail}" name = "mail" required>
            <label id="errorMail" class="col-sm-12 text-danger">{$legendMail}</label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-8">
            <input class="btn btn-default" type="submit" value="Enviar" name="submit">
        </div>
    </div>
</form>

{$modules.footer}