
{$modules.head}
{if $telegram}
<form method="post" class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="password">Código de telegram</label>
        <div class="col-sm-8">
            <input type="text" name="codeTelegram" class="form-control" id="codeTelegram" maxlength="10" placeholder="código de telegram" required>
            <label id="errorTelegram" class="col-sm-12 text-danger">{$legendTelegram}</label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-6 col-sm-8">
            <input class="btn btn-default" type="submit" value="Enviar" name="submit">
        </div>
    </div>
</form>
{else}
    <!-- Page Content -->
    <script src="{$url.global}/js/scripts.js"></script>

    <div class="center-block">
        <h2><img src="{$url.global}/images/logo.png" alt="" width = "40px" height="40px"> RECARGA EN WALLAPULPO</h2>
    </div>
    <form method="post" class="form-horizontal" onsubmit = "return validacionPurse();">

        <div class="form-group">
            <label for="importe" class="col-sm-3 control-label" id="importeLabel" style="color:{$colorUsername};">Importe</label>
            <div class="col-sm-8">
                <input type="Number" step="0.01"  class="form-control" name="importe" id = "importe">
                <label for="importe" class="col-sm-12 text-danger" id="errorImporte">{$legendImporte} {$legendError}</label>
            </div>

        </div>

        <div class="form-group">
            <label for="metodo" class="col-sm-3 control-label" id="metodo" style="color:{$colorMail};">Metodo</label>
            <div class="col-sm-9">
                <input type="radio" name="metodo" value="visa"><img src="{$url.global}/images/paymethods/visa.png" height="40" width="60">
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="paypal"><img src="{$url.global}/images/paymethods/paypal.png" height="40" width="60">
            </div>
            <div class="col-sm-offset-3 col-sm-9">
                <input type="radio" name="metodo" value="transferencia"><img src="{$url.global}/images/paymethods/transferencia.jpg" height="40" width="60">
                <label for="metodo" class="col-sm-12 text-danger" id="errorMetodo">{$legendMetodo}</label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-8">
                <input class="btn btn-default" type="submit" value="Enviar" name="submit">
            </div>
        </div>

    </form>

    <div class="row">
        <div class="col-sm-offset-1 col-lg-10">
            <h3>Búsqueda de productos.</h3>
        </div>

        <form method="post" class="form-horizontal" ENCTYPE="multipart/form-data">
            <div class="col-sm-9 col-sm-offset-1">
                <input class="form-control" id="busqueda" name ="busqueda" type="text" placeholder="Buscar producto...">
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <input class="btn btn-default" type="submit" value="Buscar" name="submit">
                </div>
            </div>
        </form>
        <div class="col-sm-offset-1 col-lg-10">
            <h3>Historial de compras.</h3>
        </div>
        <div class="col-sm-offset-1 col-sm-10">
            {if $flagCompras}
                <table class="table table-hover">
                    <tr>
                        <th>Producto</th>
                        <th>Precio</th>
                        <th>Usuario</th>
                    </tr>

                    {section name=c loop=$activos}
                        <tr>
                            <td><a href="http://g6.dev/product/{$activos[c].url}"> {$activos[c].nombre}</a></td>
                            <td>{$activos[c].precio}</td>
                            <td><a href="{$url.global}/user/{$activos[c].idusuario}">{$activos[c].idusuario}</a></td>
                        </tr>
                    {/section}
                    {section name=c loop=$inactivos}
                        <tr>
                            <td><p class="text-muted">{$inactivos[c].nombre}</p></td>
                            <td>{$inactivos[c].precio}</td>
                            <td><a href="{$url.global}/user/{$inactivos[c].idusuario}">{$inactivos[c].idusuario}</a></td>
                        </tr>
                    {/section}
                </table>
            {else}
                <p>No tienes ninguna compra registrada.</p>
            {/if}
        </div>
    </div>
{/if}





{$modules.footer}


