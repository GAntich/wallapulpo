<?php
class HomeDatabaseModel extends Model {

    public function existe($atributo,$nombre){
        $query = <<<QUERY
SELECT username
    FROM usuarios
    WHERE $atributo='$nombre';
QUERY;
        $result = $this->getAll( $query);
        if($result != null){
            return true;
        } else {
            return false;
        }
    }

    public function insertUser($nombre,$mail, $pass, $tweeter, $fotoURL, $chatTelegram, $codigoActivacion){
        $query = <<<QUERY
INSERT INTO usuarios(username, email, pass, tweeter, fotoPerfil,chatTelegram, codigoActivacion)
   VALUES('$nombre', '$mail', '$pass', '$tweeter', '$fotoURL',$chatTelegram,'$codigoActivacion' );
QUERY;
        $this->execute( $query );
        return true;
    }


    public function estaRegistrado($codigo){
        $query = <<<QUERY
SELECT username, pass
    FROM usuarios
    WHERE codigoActivacion ='$codigo' AND registro_completo = '0';
QUERY;
        $result = $this->getAll( $query);
        if($result != null){
           return $result;
        }
        return false;
    }

    public function registroCompletado($code){
        $query = <<<QUERY
UPDATE usuarios
SET registro_completo= 1
WHERE codigoActivacion = '$code';
QUERY;

        $this->execute($query);
    }

    public function saldoDisponible($nombre)
    {
        $query = <<<QUERY
SELECT dineroActual
    FROM Usuarios
    WHERE username='$nombre';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function insertarSaldo($nombre , $dineroActual)
    {
        $query = <<<QUERY
UPDATE Usuarios
    SET dineroActual=$dineroActual
    WHERE username='$nombre';
QUERY;
        $result = $this->execute( $query);
        return $result;
    }

    public function comprobarPassNombre($nombre,$password){
        $query = <<<QUERY
SELECT username
    FROM Usuarios
    WHERE username='$nombre' && pass='$password' && registro_completo='1';
QUERY;
        $result = $this->getAll( $query);
        if($result != null){
            return true;
        }
        return false;
    }

    public function comprobarPassMail($nombre,$password){
        $query = <<<QUERY
SELECT username
    FROM Usuarios
    WHERE email='$nombre' && pass='$password' && registro_completo='1';
QUERY;
        $result = $this->getAll( $query);
        if($result != null){
            return true;
        }
        return false;
    }
    public function usernamefrommail($mail){
        $query = <<<QUERY
SELECT username
    FROM Usuarios
    WHERE email='$mail';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function usernamefromid($id){
        $query = <<<QUERY
SELECT username
    FROM Usuarios
    WHERE id='$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function imageUserfromid($id){
        if(is_array($id)){
            $id = $id[0]['id'];
        }
        $query = <<<QUERY
SELECT fotoPerfil
    FROM Usuarios
    WHERE id='$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getMoney($nombre){
        $query = <<<QUERY
SELECT dineroActual
    FROM Usuarios
    WHERE username='$nombre';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }
    public function insertProduct($nombreProducto, $descripcion, $nombreUsuario, $passUsuario, $fechaCreacion, $fechaEliminacion, $imagen, $stock, $url, $precio){
        $query = <<<QUERY
INSERT INTO producto(nombre, descripcion, idUsuario, fecha_creacion, fecha_eliminacion, imagen, stock, url, precio)
   VALUES('$nombreProducto', '$descripcion', (SELECT id FROM Usuarios WHERE username = '$nombreUsuario' && pass = '$passUsuario'), '$fechaCreacion', '$fechaEliminacion','$imagen', '$stock','$url', '$precio');
QUERY;
        $this->execute( $query );
        return true;
    }

    public function getIdUser($nombreUsuario,$passUsuario){
        $query = <<<QUERY
SELECT id FROM Usuarios WHERE username = '$nombreUsuario' && pass = '$passUsuario'
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function existeUrl($url){
        $query = <<<QUERY
SELECT *
    FROM producto
    WHERE url = '$url';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function existeUser($nombre){
        $query = <<<QUERY
SELECT *
    FROM usuarios
    WHERE username = '$nombre' && registro_completo = 1;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function existeUrlAntigua($url){
        $query = <<<QUERY
SELECT *
    FROM producto
    WHERE url_ant = '$url';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getFactorExito($id){
        $query = <<<QUERY
SELECT count(*)
    FROM compras
    WHERE vendedor = '$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getComentarios($id){
        $query = <<<QUERY
SELECT c.comentario, u.username, c.fecha, u.fotoPerfil
    FROM comentarios as c, usuarios as u
    WHERE vendedor = '$id' && c.comprador = u.id;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getComentariosMios($id){
        $query = <<<QUERY
SELECT c.comentario, u.username, c.fecha, u.fotoPerfil
    FROM comentarios as c, usuarios as u
    WHERE comprador = '$id' && c.vendedor = u.id;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }
    public function insertarComentario($idcomprador,$idvendedor,$comment,$fecha){
        $query = <<<QUERY
INSERT INTO comentarios(comprador, vendedor, comentario, fecha)
   VALUES($idcomprador, $idvendedor, '$comment', '$fecha');
QUERY;
        $this->execute( $query );
        return true;
    }

    public function getProductsOrdered($id){
        $query = <<<QUERY
SELECT p.id, p.nombre, p.descripcion, p.idusuario, p.fecha_creacion, p.fecha_eliminacion, p.imagen, p.url, p.stock, p.precio,p.visitas, u.username, u.fotoPerfil
    FROM producto as p, usuarios as u
    WHERE idusuario = '$id' && fecha_eliminacion >= CURDATE() && stock > 0 && p.idusuario = u.id
    ORDER BY fecha_creacion desc,url desc;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function comprobarProducto($url){
        $query = <<<QUERY
SELECT nombre, imagen
    FROM producto
    WHERE url='$url' && stock>0 && fecha_eliminacion >= CURDATE();
QUERY;
        $result = $this->getAll( $query);
        if($result != null){
            return $result;
        }
        return false;
    }
    public function updateProduct($id, $nombre, $descripcion, $fecha_eliminacion, $img, $stock, $url_ant ,$url, $precio){
        $query = <<<QUERY
UPDATE producto
    SET nombre = '$nombre', descripcion = '$descripcion', fecha_eliminacion='$fecha_eliminacion', imagen = '$img', stock = '$stock', url_ant = '$url_ant', url = '$url', precio = '$precio'
    WHERE id='$id';
QUERY;
        $result = $this->execute( $query);
        return $result;
    }

    public function dropProduct($url){
        $query = <<<QUERY
DELETE FROM producto
WHERE url = '$url';
QUERY;
        $this->execute( $query );
        return true;
    }

    public function updateStock($id, $stock){
        $query = <<<QUERY
UPDATE producto
SET stock = '$stock'
WHERE id = '$id';
QUERY;
        $this->execute( $query );
        return true;
    }

    public function insertarCompra($producto, $comprador, $vendedor){
        $query = <<<QUERY
INSERT INTO compras(producto, comprador, vendedor)
   VALUES('$producto', '$comprador', '$vendedor');
QUERY;
        $this->execute( $query );
        return true;
    }

    public function listProducts(){
        $query = <<<QUERY
SELECT p.id, p.nombre, p.descripcion, p.idusuario,p.fecha_creacion, p.fecha_eliminacion, p.imagen, p.url, p.stock, p.precio, u.username, u.fotoPerfil
    FROM producto as p, usuarios as u
    WHERE p.stock>0 && p.fecha_eliminacion >= CURDATE() && u.id = p.idusuario
    ORDER BY fecha_creacion desc,url desc;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getCompras($idComprador){
        $query = <<<QUERY
SELECT *
    FROM compras
    WHERE comprador = '$idComprador';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getProduct($id){
        $query = <<<QUERY
SELECT *
    FROM producto
    WHERE id = '$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getLastProduct(){
        $query = <<<QUERY
SELECT *
    FROM producto
    ORDER BY id DESC
    LIMIT 1;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getMostViewed($lim){
        if($lim == -1){
            $query = <<<QUERY
SELECT *
    FROM producto
    WHERE stock>0 && fecha_eliminacion >= CURDATE()
    ORDER BY visitas DESC;
QUERY;
        }else{
            $query = <<<QUERY
SELECT *
    FROM producto
    WHERE stock>0 && fecha_eliminacion >= CURDATE()
    ORDER BY visitas DESC
    LIMIT $lim;
QUERY;
        }
        $result = $this->getAll( $query);
        return $result;
    }

    public function getLastImages(){
        $query = <<<QUERY
SELECT imagen
    FROM producto
    WHERE imagen != 'http://g6.dev/images/products/none-100x100.jpg' && imagen IS NOT NULL
    ORDER BY id DESC
    LIMIT 5;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getVisitas($id){
        $query = <<<QUERY
SELECT visitas
FROM producto
WHERE id = '$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function setVisitas($id, $visitas){
        $query = <<<QUERY
UPDATE producto
SET visitas = '$visitas'
WHERE id = '$id';
QUERY;
        $this->execute( $query );
        return true;
    }
    public function correoVendedor($id){
        $query = <<<QUERY
SELECT email
    FROM usuarios
    WHERE id='$id';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function insertarCodigoPassword($mail, $code){
    $query = <<<QUERY
UPDATE usuarios
SET codigoPassword = '$code'
WHERE email = '$mail';
QUERY;
    $this->execute( $query );
    return true;
    }

    public function infoUsuario($code){
        $query = <<<QUERY
SELECT *
    FROM usuarios
    WHERE codigoPassword='$code';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }
    public function updatePassword($id, $password){
        $query = <<<QUERY
UPDATE usuarios
SET pass = '$password'
WHERE id = '$id';
QUERY;
        $this->execute( $query );
        return true;
    }

    public function getVentas($id){
        $query = <<<QUERY
SELECT count(*)
    FROM compras
    WHERE producto = $id;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getUserVentas($id){
        $query = <<<QUERY
SELECT count(*)
    FROM compras
    WHERE vendedor = $id;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }


    public function HayComentarios($quien, $aquien){
        $query = <<<QUERY
SELECT *
    FROM comentarios
    WHERE comprador = $quien && vendedor = $aquien;
QUERY;
        $result = $this->getAll( $query);
        if($result == null) return false;
        return true;

    }

    public function HayVentas($quien, $aquien){
        $query = <<<QUERY
SELECT *
    FROM compras
    WHERE comprador = $quien && vendedor = $aquien;
QUERY;
        $result = $this->getAll( $query);

        if($result == null) return false;
        return true;

    }

    public function updateComment($comprador, $vendedor, $comment, $fecha){
        $query = <<<QUERY
UPDATE comentarios
SET comentario = '$comment', fecha = '$fecha'
WHERE comprador = $comprador && vendedor = $vendedor ;
QUERY;
        $this->execute( $query );
        return true;

    }

    public function dropComment($comprador, $vendedor){
        $query = <<<QUERY
DELETE FROM comentarios
WHERE comprador = '$comprador' && vendedor = $vendedor;
QUERY;
        $this->execute( $query );
        return true;
    }

    public function getTelegramChatId($nombre, $password){
        $query = <<<QUERY
SELECT count(*),chatTelegram
    FROM usuarios
    WHERE username = '$nombre' && pass = '$password' && chatTelegram is not null;
QUERY;
        $result = $this->getAll( $query);
        if($result[0]['count(*)'] == 0) return false;
        return $result;
    }

    public function getTelegramFromId($id){
        $query = <<<QUERY
SELECT count(*),chatTelegram
    FROM usuarios
    WHERE id = '$id' && chatTelegram is not null;
QUERY;
        $result = $this->getAll( $query);
        if($result[0]['count(*)'] == 0) return false;
        return $result;
    }

    public function getCodeTelegram($username, $pass){
        $query = <<<QUERY
SELECT codigoTelegram
    FROM usuarios
    WHERE username = '$username' && pass = '$pass';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function updateCodeTelegram($code, $username, $pass){
        $query = <<<QUERY
UPDATE usuarios
SET codigoTelegram = '$code'
WHERE username = '$username' && pass = '$pass';
QUERY;
        $this->execute( $query );
        return true;
    }
    public function getMyConversations($id){
        $query = <<<QUERY
SELECT conversation, title, user1, user2, message, timestamp
    FROM mensajes
    WHERE user1 = $id || user2 = $id
    GROUP BY conversation;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function getLastMessageAndFlags($conversation){
        $query = <<<QUERY
SELECT message, user1, user2, unread
    FROM mensajes
    WHERE conversation = '$conversation'
    ORDER BY id DESC
    LIMIT 1;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function existsConversation($conversation){
        $query = <<<QUERY
SELECT title, user1, user2, message, timestamp, unread
    FROM mensajes
    WHERE conversation = '$conversation'
    ORDER BY id DESC;
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }
    public function newMessageinConversation($conversation,$title,$user1,$user2,$message,$timestamp){
        $query = <<<QUERY
INSERT INTO mensajes(conversation,title,user1,user2,message,timestamp,unread)
   VALUES('$conversation','$title',$user1,$user2,'$message','$timestamp',1);
QUERY;
        $this->execute( $query );
        return true;
    }

    public function getIdfromUsername($username){
        $query = <<<QUERY
SELECT id
    FROM usuarios
    WHERE username = '$username';
QUERY;
        $result = $this->getAll( $query);
        return $result;
    }

    public function setReadToFalse($conversation,$userid){
        $query = <<<QUERY
UPDATE mensajes
SET unread = 0
WHERE conversation = '$conversation' && user2 = $userid;
QUERY;
        $this->execute( $query );
        return true;
    }
    public function hasUnreadMessages($id){
        $query = <<<QUERY
SELECT count(unread)
    FROM mensajes
    WHERE user2 = $id && unread = 1;
QUERY;
        $result = $this->getAll($query);
        return $result;
    }
    public function repeatPassword($mail, $repeat){
        $query = <<<QUERY
UPDATE usuarios
SET repeatPassword = '$repeat'
WHERE email='$mail';
QUERY;
        $this->execute( $query );
        return true;
    }

}