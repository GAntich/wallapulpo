
CREATE DATABASE wallapulpo;
USE wallapulpo;


-- Base de datos: `wallapulpo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `comprador` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `comentario` varchar(255) NOT NULL,
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`comprador`, `vendedor`, `comentario`, `id`, `fecha`) VALUES
(3, 2, 'El dron de hello kitty mola mazo!', 1, '2016-05-14'),
(2, 3, 'Qué guapo! El dron digo...', 2, '2016-05-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `producto` int(11) NOT NULL,
  `comprador` int(11) NOT NULL,
  `vendedor` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`producto`, `comprador`, `vendedor`, `id`) VALUES
(4, 3, 2, 1),
(4, 3, 2, 2),
(4, 3, 2, 3),
(4, 3, 2, 4),
(4, 3, 2, 5),
(4, 3, 2, 6),
(4, 3, 2, 7),
(4, 3, 2, 8),
(4, 3, 2, 9),
(4, 3, 2, 10),
(4, 3, 2, 11),
(4, 3, 2, 12),
(4, 3, 2, 13),
(4, 3, 2, 14),
(4, 3, 2, 15),
(4, 3, 2, 16),
(4, 3, 2, 17),
(4, 3, 2, 18),
(4, 3, 2, 19),
(4, 1, 2, 20),
(4, 3, 2, 21),
(4, 3, 2, 22),
(4, 3, 2, 23),
(4, 3, 2, 24),
(4, 3, 2, 25),
(4, 3, 2, 26),
(4, 3, 2, 27),
(4, 3, 2, 28),
(4, 3, 2, 29),
(4, 3, 2, 30),
(4, 3, 2, 31),
(4, 3, 2, 32),
(4, 3, 2, 33),
(4, 3, 2, 34),
(4, 3, 2, 35),
(4, 3, 2, 36),
(4, 3, 2, 37),
(4, 3, 2, 38),
(4, 3, 2, 39),
(4, 3, 2, 40),
(4, 3, 2, 41),
(4, 3, 2, 42),
(4, 3, 2, 43),
(4, 3, 2, 44),
(4, 3, 2, 45),
(4, 3, 2, 46),
(4, 3, 2, 47),
(4, 3, 2, 48),
(4, 3, 2, 49),
(4, 3, 2, 50),
(4, 3, 2, 51),
(4, 3, 2, 52),
(4, 3, 2, 53),
(4, 3, 2, 54),
(4, 3, 2, 55),
(4, 3, 2, 56),
(4, 3, 2, 57),
(4, 3, 2, 58),
(4, 3, 2, 59),
(4, 3, 2, 60),
(4, 3, 2, 61),
(4, 3, 2, 62),
(4, 3, 2, 63),
(4, 3, 2, 64),
(4, 3, 2, 65),
(4, 3, 2, 66),
(4, 3, 2, 67),
(4, 3, 2, 68),
(4, 3, 2, 69),
(4, 3, 2, 70),
(4, 3, 2, 71),
(4, 3, 2, 72),
(4, 3, 2, 73),
(4, 3, 2, 74),
(4, 3, 2, 75),
(4, 3, 2, 76),
(4, 3, 2, 77),
(4, 3, 2, 78),
(4, 3, 2, 79),
(4, 3, 2, 80),
(4, 3, 2, 81),
(4, 3, 2, 82),
(4, 3, 2, 83),
(4, 3, 2, 84),
(4, 3, 2, 85),
(4, 3, 2, 86),
(4, 3, 2, 87),
(4, 3, 2, 88),
(4, 3, 2, 89),
(4, 3, 2, 90),
(4, 3, 2, 91),
(4, 3, 2, 92),
(4, 3, 2, 93),
(4, 3, 2, 94),
(4, 3, 2, 95),
(4, 3, 2, 96),
(4, 3, 2, 97),
(4, 3, 2, 98),
(4, 3, 2, 99),
(4, 3, 2, 100),
(4, 3, 2, 101),
(5, 3, 2, 102),
(15, 2, 3, 103);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `conversation` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `user1` int(11) DEFAULT NULL,
  `user2` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `timestamp` date NOT NULL,
  `unread` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `conversation`, `title`, `user1`, `user2`, `message`, `timestamp`, `unread`) VALUES
(1, 'Sobre_el_dron_de_Hello_Kitty', 'Sobre el dron de Hello Kitty', 3, 2, '<p>Buenas Alballauro,</p><p>he visto que vendes el dron de Hello Kitty. Hace mucho tiempo que lo ando buscando. Me explicas un poco cómo funciona?</p><p>Gracias!</p>', '2016-05-14', 0),
(2, 'Sobre_el_dron_de_Hello_Kitty', 'Sobre el dron de Hello Kitty', 2, 3, '<p>Buenas!</p><p>Tu guias al dron con la mano, y el se va apartando. Lo más complicado es cogerlo para guardarlo en su caja, ya que no se deja coger!</p>', '2016-05-14', 0),
(3, 'Sobre_el_dron_de_Hello_Kitty', 'Sobre el dron de Hello Kitty', 3, 2, '<p>Vale, perfecto!</p><p>Cuándo puedo pasar a recoger-lo?</p>', '2016-05-14', 0),
(4, 'Sobre_el_dron_de_Hello_Kitty', 'Sobre el dron de Hello Kitty', 2, 3, '<p>Cuando te vaya bien pásate por la Universidad!</p>', '2016-05-14', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_eliminacion` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `visitas` int(11) NOT NULL DEFAULT '0',
  `url_ant` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `precio` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `idusuario`, `fecha_creacion`, `fecha_eliminacion`, `imagen`, `visitas`, `url_ant`, `url`, `stock`, `precio`) VALUES
(1, 'DRagON volador', '<p>Huid, es un fúria nocturna!!</p>', 1, '2016-05-14', '2017-01-05', 'http://g6.dev/images/products/silvia-DRagON volador-400x300-dron1.jpg', 628, NULL, 'DRagON_volador', 1, 1000000),
(2, 'A8 quadcopter', '<p>Se trata de un dron con una cámara de 2 mpíxels rojo.</p>', 1, '2016-05-14', '2016-09-30', 'http://g6.dev/images/products/silvia-A8 quadcopter-400x300-dron2.jpg', 1, NULL, 'A8_quadcopter', 12, 31.15),
(3, 'e-turbine tb 250', '<p>Es un dron tipo lego que parece hecho a piezas.</p>', 1, '2016-05-14', '2016-07-22', 'http://g6.dev/images/products/silvia-e-turbine tb 250-400x300-dron3.jpg', 1, NULL, 'e-turbine_tb_250', 12, 177.99),
(4, 'dron de hello kitty', '<p>Es un dron rosita, muy chachi, de estos que pones debajo la mano y suben para evitarlo. Ideal si tienes hijos para que se entretengan.</p>', 2, '2016-05-14', '2016-09-02', 'http://g6.dev/images/products/alballauro-dron de hello kitty-400x300-dron de hello kitty.jpg', 147, NULL, 'dron_de_hello_kitty', 20, 12),
(5, 'quanum followme aerial action', '<p>Un dron rojo y negro de 4 aspas que vuela muy bien.</p>', 2, '2016-05-14', '2016-10-28', 'http://g6.dev/images/products/alballauro-quanum followme aerial action-400x300-dron4.jpg', 3, NULL, 'quanum_followme_aerial_action', 0, 403.93),
(6, 'dys 250 full carbon fiber', '<p>Dron hecho completamente de fibra de carbono. No será el más bonito, pero es ligero como el viento!</p>', 2, '2016-05-14', '2016-12-23', 'http://g6.dev/images/products/alballauro-dys 250 full carbon fiber-400x300-dron5.jpg', 2, NULL, 'dys_250_full_carbon_fiber', 46, 88.99),
(7, 'udi rc free loop', '<p>Dron negro que puede hacer vueltas en el aire.</p>', 2, '2016-05-14', '2016-11-04', 'http://g6.dev/images/products/alballauro-udi rc free loop-400x300-dron6.jpg', 1, NULL, 'udi_rc_free_loop', 56, 34.45),
(8, '9 eagles feng FPV quadcopter', '<p>Es un dron amarillo con una estética muy bonita y una gran estabilidad en el aire.</p>', 2, '2016-05-14', '2016-09-30', 'http://g6.dev/images/products/alballauro-9 eagles feng FPV quadcopter-400x300-dron7.jpg', 1, NULL, '9_eagles_feng_FPV_quadcopter', 37, 73.25),
(9, 'syma x8c venture', '<p>Dron negro con cámara incorporada que se camufla muy bien en espacios oscuros.</p>', 2, '2016-05-14', '2016-07-27', 'http://g6.dev/images/products/alballauro-syma x8c venture-400x300-dron8.jpg', 1, NULL, 'syma_x8c_venture', 78, 84.85),
(10, 'pioneer ufo 509v', '<p>Dron blanco con cámara incorporada.</p>', 2, '2016-05-14', '2016-10-07', 'http://g6.dev/images/products/alballauro-pioneer ufo 509v-400x300-dron9.jpg', 1, NULL, 'pioneer_ufo_509v', 1, 49.47),
(11, 'triphibious quadcopter', '<p>Es un <b style="background-color: rgb(255, 255, 0);">dron</b> y un <span style="background-color: rgb(255, 255, 0);">coche</span> teledirigidos a la vez.</p>', 2, '2016-05-14', '2016-11-03', 'http://g6.dev/images/products/alballauro-triphibious quadcopter-400x300-dron11.jpg', 1, NULL, 'triphibious_quadcopter', 12, 21.56),
(12, 'pioneew ufo 509V', '<p>Dron negro con pantalla en el mando.</p>', 2, '2016-05-14', '2016-09-22', 'http://g6.dev/images/products/alballauro-pioneew ufo 509V-400x300-dron10.jpg', 1, NULL, 'pioneew_ufo_509V', 64, 76.87),
(13, 'h11d', '<p>Dron rojo muy finito pero con gran velocidad.</p>', 2, '2016-05-14', '2016-12-30', 'http://g6.dev/images/products/alballauro-h11d-400x300-dron12.jpg', 1, NULL, 'h11d', 5, 73.07),
(14, 'udi-rc RU818A', '<p>Uno de los típicos drones con 4 aspas redondas.</p>', 2, '2016-05-14', '2016-11-17', 'http://g6.dev/images/products/alballauro-udi-rc RU818A-400x300-dron13.jpg', 1, NULL, 'udi-rc_RU818A', 65, 55.49),
(15, 'pocket drone 4CH', '<p>Uno de esos drones chiquititos que te caben en el bolsillo.</p>', 3, '2016-05-14', '2016-10-07', 'http://g6.dev/images/products/guillermoantich-pocket-drone-4CH-400x300-dron pequeño.jpg', 2, NULL, 'pocket_drone_4CH', 31, 13.42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `tweeter` varchar(255) DEFAULT NULL,
  `fotoPerfil` varchar(255) DEFAULT NULL,
  `activo` int(10) DEFAULT '0',
  `codigoActivacion` varchar(255) DEFAULT NULL,
  `dineroActual` float DEFAULT '0',
  `registro_completo` int(10) NOT NULL DEFAULT '0',
  `chatTelegram` varchar(255) DEFAULT NULL,
  `codigoPassword` varchar(255) NOT NULL,
  `codigoTelegram` varchar(255) DEFAULT NULL,
  `repeatPassword` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `email`, `pass`, `tweeter`, `fotoPerfil`, `activo`, `codigoActivacion`, `dineroActual`, `registro_completo`, `chatTelegram`, `codigoPassword`, `codigoTelegram`, `repeatPassword`) VALUES
(1, 'silvia', 'scano1993@gmail.com', 'asdf3D', '@scano', 'http://g6.dev/images/avatar/silvia4646658-Silvia.png', 0, 'f402472cfd59a50370bc6d1c273b268af04a90bb', 136, 1, '182069762', '', 'jcgrnvg4', 0),
(2, 'alballauro', 'alballauro94@gmail.com', 'asdf3D', '@alballauro94', 'http://g6.dev/images/avatar/alballauro1962641-Alba.png', 0, 'cf4ccc0648f9ae087ec51a41055d0704e14958fb', 1634.51, 1, NULL, '', NULL, 0),
(3, 'guillermoantich', 'guillermo@gmail.com', 'asdf3D', '@guillermo', 'http://g6.dev/images/avatar/guillermoantich1428536-Guille.png', 0, '76e7f6942bd8a10c0c813c3694c05466ef75afd7', 109.49, 1, NULL, '', NULL, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;